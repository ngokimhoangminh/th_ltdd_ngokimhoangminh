package com.example.customlistview;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private Context context;
    private List<Cauthu> cauthuList;

    public CustomAdapter(Context context, List<Cauthu> cauthuList) {
        this.context = context;
        this.cauthuList = cauthuList;
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        ImageView imgchandung;
        TextView txtTen2;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgchandung=itemView.findViewById(R.id.imageviewHinh2);
            txtTen2=itemView.findViewById(R.id.textviewTen2);
            imgchandung.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
           contextMenu.add(this.getAdapterPosition(),121,0,"Delete Item");
            contextMenu.add(this.getAdapterPosition(),121,0,"Add Item");
        }

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.layout_custom,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Cauthu cauthu=cauthuList.get(position);
        holder.imgchandung.setImageResource(cauthu.getChandung());
        holder.txtTen2.setText(cauthu.getTen());
    }

    @Override
    public int getItemCount() {
        return cauthuList.size();
    }


}
