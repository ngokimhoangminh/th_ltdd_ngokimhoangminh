package com.example.customlistview;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    ImageView imgBack,imgSearch,imgList;
    TextView txtHeader;
    private Toolbar toolbar;
    ArrayList<Cauthu> arraycauthu;
    private MyAdapter myAdapter;
    RecyclerView rcylist;
    ListView lvCauthu;
    public Uri imageUri;
    public ImageView imgUpdate;
    public static List<Cauthu> UserSelection=new ArrayList<Cauthu>();
    public static boolean isActionMode=false;
    public static ActionMode actionModes=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Anhxa();

        rcylist=(RecyclerView) findViewById(R.id.listviewNgang);
        LinearLayoutManager manager1=new LinearLayoutManager(this);
        manager1.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcylist.setLayoutManager(manager1);
        CustomAdapter customAdapter=new CustomAdapter(this,arraycauthu);
        rcylist.setAdapter(customAdapter);

        lvCauthu.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE_MODAL);
        lvCauthu.setMultiChoiceModeListener(modeListener);
        //thiết lập adapter cho list dọc
        myAdapter=new MyAdapter(this,R.layout.layout_cauthu,arraycauthu);
        lvCauthu.setAdapter(myAdapter);
        //registerForContextMenu(lvCauthu);
        /*
        LinearLayoutManager manager2=new LinearLayoutManager(this);
        lvCauthu.setLayoutManager(manager2);
        cauThuAdapter=new CauThuAdapter(this,arraycauthu);
        lvCauthu.setAdapter(cauThuAdapter);*/
        ///

    }



    AbsListView.MultiChoiceModeListener modeListener = new AbsListView.MultiChoiceModeListener() {
        @Override
        public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

        }

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater= actionMode.getMenuInflater();
            inflater.inflate(R.menu.menu_context,menu);
            isActionMode=true;
            actionModes=actionMode;
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId())
            {
                case R.id.menuXoa:
                    myAdapter.removeItems(UserSelection);
                    actionMode.finish();
                    displayMessage("Item deleted....");
                    return true;
                case R.id.menuThem:
                    Them();
                    return true;
                default:
                    return false;
            }

        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            isActionMode=false;
            actionModes=null;
            UserSelection.clear();
        }
    };
    public void Them()
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.layout_add);
        dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        final EditText edtName=(EditText) dialog.findViewById(R.id.EdittextName);
        final EditText edtPrice=(EditText) dialog.findViewById(R.id.EditTextPrice);
        Button btnCancel=(Button) dialog.findViewById(R.id.ButtonCancel);
        Button btnThem=(Button) dialog.findViewById(R.id.ButtonThem);
        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String ten=edtName.getText().toString().trim();
                final String gia=edtPrice.getText().toString().trim();
                arraycauthu.add(new Cauthu(ten,gia,R.drawable.barcelona2,R.drawable.messi2));
                myAdapter.notifyDataSetChanged();
                lvCauthu.setAdapter(myAdapter);
                Toast.makeText(MainActivity.this,"Thêm thành công",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
    public void Sua(final  int position)
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.layout_edit);
        dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog

        final Cauthu cauthu=arraycauthu.get(position);
        final EditText edtName=(EditText) dialog.findViewById(R.id.EdittextName);
        final EditText edtPrice=(EditText) dialog.findViewById(R.id.EditTextPrice);
        Button btnSave=(Button) dialog.findViewById(R.id.ButtonLuu);
        Button btnHuy=(Button) dialog.findViewById(R.id.ButtonHuy);
        edtName.setText(arraycauthu.get(position).getTen());
        edtPrice.setText(arraycauthu.get(position).getNamsinh());
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String ten=edtName.getText().toString().trim();
                final String gia=edtPrice.getText().toString().trim();
                final String hinh;
                cauthu.setTen(ten);
                cauthu.setNamsinh(gia);

                //cauthu.setQuocqua(R.drawable.hinh);
                myAdapter.notifyDataSetChanged();
                lvCauthu.setAdapter(myAdapter);
                Toast.makeText(MainActivity.this,"Cập nhật thành công",Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    /*
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId())
        {
            case 1:

                cauThuAdapter.removeItem(item.getItemId());
                displayMessage("Item deleted....");
                return true;
            case 2:
                displayMessage("Item add....");
                return true;
            case 3:
                Toast.makeText(MainActivity.this,"Item update...",Toast.LENGTH_SHORT).show();
            default:
                return super.onContextItemSelected(item);
        }

    }*/
    private  void displayMessage(String message)
    {
        Snackbar.make(findViewById(R.id.rootView),message,Snackbar.LENGTH_SHORT).show();
    }
    public  void Anhxa()
    {
        lvCauthu= (ListView) findViewById(R.id.listviewCauthu);
        arraycauthu=new ArrayList<>();
        arraycauthu.add(new Cauthu("L.Messi","$160M",R.drawable.barcelona2,R.drawable.messi2));
        arraycauthu.add(new Cauthu("Neymar","$260M",R.drawable.parisaintgermain,R.drawable.neymar2));
        arraycauthu.add(new Cauthu("Griezmann","$150M",R.drawable.barcelona2,R.drawable.griezman2));
        arraycauthu.add(new Cauthu("O.Dembele","$140M",R.drawable.barcelona2,R.drawable.dembele));
        arraycauthu.add(new Cauthu("K.Mbappe","$240M",R.drawable.parisaintgermain,R.drawable.mbappe));
        arraycauthu.add(new Cauthu("G.Bale","$80M",R.drawable.realmadrid,R.drawable.bale2));
        arraycauthu.add(new Cauthu("Coutinho","$165M",R.drawable.barcelona2,R.drawable.coutinho2));
        arraycauthu.add(new Cauthu("Ronaldo","$165M",R.drawable.realmadrid,R.drawable.ronaldo2));
    }
}