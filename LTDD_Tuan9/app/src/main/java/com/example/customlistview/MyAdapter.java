package com.example.customlistview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter  extends BaseAdapter {
    private MainActivity context;
    private int layout;
    private List<Cauthu> cauthuList;

    public MyAdapter(MainActivity context, int layout, List<Cauthu> cauthuList) {
        this.context = context;
        this.layout = layout;
        this.cauthuList = cauthuList;
    }

    @Override
    public int getCount() {
        return cauthuList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        //view chứa layout, layout nào hiển thị mỗi dòng
        view=inflater.inflate(layout,null);
        //CircleImageView CimgChandung=(CircleImageView) view.findViewById(R.id.CircleImageView);
        ImageView imgChandung=(ImageView) view.findViewById(R.id.imageviewHinh);
        ImageView imgQuocgia=(ImageView) view.findViewById(R.id.imageviewClub);
        TextView txtTen=(TextView) view.findViewById(R.id.textviewTen);
        TextView txtNamsinh=(TextView) view.findViewById(R.id.textviewGia);
        CheckBox cbXoa=(CheckBox) view.findViewById(R.id.checkbox);
        ImageButton imgEdit=(ImageButton) view.findViewById(R.id.ImageButtonEdit);
        cbXoa.setTag(i);
        Cauthu cauthu=cauthuList.get(i);
        txtTen.setText(cauthu.getTen());
        txtNamsinh.setText(cauthu.getNamsinh());
        imgChandung.setImageResource(cauthu.getChandung());
        imgQuocgia.setImageResource(cauthu.getQuocqua());

        if(MainActivity.isActionMode)
        {
            cbXoa.setVisibility(view.VISIBLE);
            imgEdit.setVisibility(view.VISIBLE);
        }
        else
        {
            cbXoa.setVisibility(view.GONE);
            imgEdit.setVisibility(view.GONE);
        }
        cbXoa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int position=(int) compoundButton.getTag();
                if(MainActivity.UserSelection.contains(cauthuList.get(i)))
                {
                    MainActivity.UserSelection.remove(cauthuList.get(i));
                }
                else
                {
                    MainActivity.UserSelection.add(cauthuList.get(i));
                }
                MainActivity.actionModes.setTitle(MainActivity.UserSelection.size()+" items selected..");
            }
        });
        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.Sua(i);
            }
        });
        return view;

    }
    public void removeItems(List<Cauthu> items){
        for(Cauthu item : items)
        {
            cauthuList.remove(item);
        }
        notifyDataSetChanged();
    }
}
