package com.example.customlistview;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class CauThuAdapter extends RecyclerView.Adapter<CauThuAdapter.MyViewHolder> {
    private Context context;
    private List<Cauthu> cauthus;
    private  MainActivity mainActivity;
    public CauThuAdapter(Context context, List<Cauthu> cauthus) {
        this.context = context;
        this.cauthus = cauthus;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        ImageView imgchandung,imgClub;
        LinearLayout linearLayout;
        TextView txtTen,txtGia;
        RelativeLayout rtl;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            rtl=itemView.findViewById(R.id.background);
            imgchandung=itemView.findViewById(R.id.imageviewHinh);
            imgClub=itemView.findViewById(R.id.imageviewClub);
            txtTen=itemView.findViewById(R.id.textviewTen);
            txtGia=itemView.findViewById(R.id.textviewGia);
            rtl.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.setHeaderTitle("Select Option");
            contextMenu.setHeaderIcon(R.drawable.league);
            contextMenu.add(this.getAdapterPosition(),1,0,"Delete Item");
            contextMenu.add(this.getAdapterPosition(),2,1,"Add Item");
            contextMenu.add(this.getAdapterPosition(),3,2,"Update Item");
        }
    }
    public void removeItem(int position)
    {
        cauthus.remove(position);
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.layout_cauthu,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Cauthu cauthu=cauthus.get(position);
        holder.imgchandung.setImageResource(cauthu.getChandung());
        holder.txtTen.setText(cauthu.getTen());
        holder.imgClub.setImageResource(cauthu.getQuocqua());
        holder.txtGia.setText(cauthu.getNamsinh());
        if(mainActivity.isActionMode)
        {
            Anim anim=new Anim(100,holder.linearLayout);
            anim.setDuration(300);
            holder.linearLayout.setAnimation(anim);
        }
        else {
            Anim anim=new Anim(0,holder.linearLayout);
            anim.setDuration(300);
            holder.linearLayout.setAnimation(anim);
        }


    }

    @Override
    public int getItemCount() {
        return cauthus.size();
    }
    class Anim extends Animation{
        private  int width,startWidth;
        private View view;

        public Anim(int width,  View view) {
            this.width = width;
            this.startWidth = view.getWidth();
            this.view = view;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {

            int newWidth=startWidth + (int) ((width-startWidth)*interpolatedTime);
            view.getLayoutParams().width=newWidth;
            view.requestLayout();

            super.applyTransformation(interpolatedTime, t);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }

}
