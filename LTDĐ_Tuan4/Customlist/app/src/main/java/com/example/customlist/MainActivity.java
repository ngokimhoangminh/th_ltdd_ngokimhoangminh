package com.example.customlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView lvCauthu;
    ArrayList<Cauthu> arraycauthu;
    CauthuAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        adapter=new CauthuAdapter(this,R.layout.layout_cauthu,arraycauthu);
        lvCauthu.setAdapter(adapter);
    }
    public  void Anhxa()
    {
        lvCauthu=(ListView) findViewById(R.id.listviewCauthu);
        arraycauthu=new ArrayList<>();
        arraycauthu.add(new Cauthu("N.K Hoàng Minh","January 14-2000",R.drawable.vietnam,R.drawable.pele));
        arraycauthu.add(new Cauthu("Diego Madarona","October 30-1952",R.drawable.argentina,R.drawable.maradona));
        arraycauthu.add(new Cauthu("Johan Cruff","April 25-1947",R.drawable.halan,R.drawable.cruff));
        arraycauthu.add(new Cauthu("Lionel Messi","June 24-1987",R.drawable.argentina,R.drawable.messi));
        arraycauthu.add(new Cauthu("Cr.Ronaldo","May 02-1943",R.drawable.bodaonha,R.drawable.ronaldo));
        arraycauthu.add(new Cauthu("Ronaldo De Lima","September 18-1976",R.drawable.brazil,R.drawable.ronaldodelima));
    }
}