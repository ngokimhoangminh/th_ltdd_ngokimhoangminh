package com.example.activity;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button btOpenChild;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btOpenChild=(Button) findViewById(R.id.openChild);
        btOpenChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doOpenChildActivity();

            }
        });
    }
    public void doOpenChildActivity()
    {
        Intent myIntent=new Intent(MainActivity.this, ChildActivity.class);
        startActivity(myIntent);
    }

}