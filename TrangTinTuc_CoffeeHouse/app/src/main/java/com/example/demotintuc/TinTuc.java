package com.example.demotintuc;

public class TinTuc {
    String hinh;
    String ten;
    String noidung;
    String hinhthuc;

    public TinTuc() {
    }

    public TinTuc(String hinh, String ten, String noidung, String hinhthuc) {
        this.hinh = hinh;
        this.ten = ten;
        this.noidung = noidung;
        this.hinhthuc = hinhthuc;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getNoidung() {
        return noidung;
    }

    public void setNoidung(String noidung) {
        this.noidung = noidung;
    }

    public String getHinhthuc() {
        return hinhthuc;
    }

    public void setHinhthuc(String hinhthuc) {
        this.hinhthuc = hinhthuc;
    }
}
