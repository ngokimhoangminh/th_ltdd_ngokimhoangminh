package com.example.demotintuc;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<TinTuc> arrTinTuc;
    TinTucAdapter adapter;
    DatabaseReference mData;
    RecyclerView reUudai,reCapnhat;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        arrTinTuc=new ArrayList<TinTuc>();
        reUudai=(RecyclerView) findViewById(R.id.recycleviewUudai);
        reCapnhat=(RecyclerView) findViewById(R.id.recycleviewCapnhat);
        LoadUudai();
        LoadCapnhat();
    }
    private void LoadUudai()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("tintuc");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot:snapshot.getChildren())
                {
                    TinTuc p=dataSnapshot.getValue(TinTuc.class);
                    arrTinTuc.add(p);
                }
                adapter=new TinTucAdapter(MainActivity.this,arrTinTuc);
                LinearLayoutManager manager=new LinearLayoutManager(MainActivity.this);
                manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                reUudai.setLayoutManager(manager);
                reUudai.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this,"error....",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadCapnhat()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("tintuc");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot:snapshot.getChildren())
                {
                    TinTuc p=dataSnapshot.getValue(TinTuc.class);
                    arrTinTuc.add(p);
                }
                adapter=new TinTucAdapter(MainActivity.this,arrTinTuc);
                LinearLayoutManager manager=new LinearLayoutManager(MainActivity.this);
                manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                reCapnhat.setLayoutManager(manager);
                reCapnhat.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(MainActivity.this,"error....",Toast.LENGTH_LONG).show();
            }
        });
    }
}