package com.example.demotintuc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.List;

public class TinTucAdapter extends RecyclerView.Adapter<TinTucAdapter.MyViewHolder> {
    public Context context;
    public List<TinTuc> tinTucList;

    public TinTucAdapter(Context context, List<TinTuc> tinTucList) {
        this.context = context;
        this.tinTucList = tinTucList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        TextView txt_item_ten,txt_item_noidung,txt_item_hinhthuc;
        ImageView img_item_hinh;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_item_noidung=(TextView) itemView.findViewById(R.id.text_item_noidung);
            txt_item_ten=(TextView) itemView.findViewById(R.id.text_item_ten);
            txt_item_hinhthuc=(TextView) itemView.findViewById(R.id.text_item_hinhthuc);
            img_item_hinh=(ImageView) itemView.findViewById(R.id.image_item_hinh);
        }
    }

    @NonNull
    @Override
    public TinTucAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.tintuc_item,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TinTucAdapter.MyViewHolder holder, int position) {
        TinTuc tinTuc=tinTucList.get(position);
        holder.txt_item_ten.setText(tinTuc.getTen());
        holder.txt_item_noidung.setText(tinTuc.getNoidung());
        holder.txt_item_hinhthuc.setText(tinTuc.getHinhthuc());
        Picasso.get().load(tinTuc.getHinh()).into(holder.img_item_hinh);
    }

    @Override
    public int getItemCount() {
        return tinTucList.size();
    }
}
