package com.example.layouttonghop;

public class Cauthu {
    private String Ten;
    private String namsinh;
    private int quocqua;
    private int chandung;

    public Cauthu(String ten, String namsinh, int quocqua, int chandung) {
        Ten = ten;
        this.namsinh = namsinh;
        this.quocqua = quocqua;
        this.chandung = chandung;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public String getNamsinh() {
        return namsinh;
    }

    public void setNamsinh(String namsinh) {
        this.namsinh = namsinh;
    }

    public int getQuocqua() {
        return quocqua;
    }

    public void setQuocqua(int quocqua) {
        this.quocqua = quocqua;
    }

    public int getChandung() {
        return chandung;
    }

    public void setChandung(int chandung) {
        this.chandung = chandung;
    }
}

