package com.example.layouttonghop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

public class CustomListActivity extends Activity {
    ListView lvCauthu;
    ArrayList<Cauthu> arraycauthu;
    CauthuAdapter adapter;
    ImageView imgLogout,imgCustomlist,imgProfile;
    TextView txtProfile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customlist);
        Anhxa();
        adapter=new CauthuAdapter(this,R.layout.layout_cauthu,arraycauthu);
        lvCauthu.setAdapter(adapter);
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(CustomListActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
        txtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(CustomListActivity.this, ProfileActivity.class);
                startActivity(myIntent);
            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(CustomListActivity.this, ProfileActivity.class);
                startActivity(myIntent);
            }
        });
        imgCustomlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CustomListActivity.this,"Bạn đã ở CustomList ",Toast.LENGTH_LONG).show();
            }
        });

    }
    public  void Anhxa()
    {
        txtProfile=(TextView) findViewById(R.id.profileText);
        imgProfile=(ImageView) findViewById(R.id.profile);
        imgCustomlist=(ImageView) findViewById(R.id.customlist);
        imgLogout=(ImageView) findViewById(R.id.logout);
        lvCauthu=(ListView) findViewById(R.id.listviewCauthu);
        arraycauthu=new ArrayList<>();
        arraycauthu.add(new Cauthu("L.Messi","$160M",R.drawable.barcelona2,R.drawable.messi));
        arraycauthu.add(new Cauthu("Neymar","$180M",R.drawable.parisaintgermain,R.drawable.neymar));
        arraycauthu.add(new Cauthu("Griezmann","$150M",R.drawable.barcelona2,R.drawable.griezman));
        arraycauthu.add(new Cauthu("Gareth Bale","$80M",R.drawable.realmadrid,R.drawable.bale));
        arraycauthu.add(new Cauthu("Coutinho","$165M",R.drawable.barcelona2,R.drawable.coutinho));
        arraycauthu.add(new Cauthu("Cr.Ronaldo","$100M",R.drawable.juventus,R.drawable.ronaldo));

    }
}
