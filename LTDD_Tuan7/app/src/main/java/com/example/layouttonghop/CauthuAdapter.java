package com.example.layouttonghop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CauthuAdapter extends BaseAdapter {
    private Context context;
    private int layout;
    private List<Cauthu> cauthuList;

    public CauthuAdapter(Context context, int layout, List<Cauthu> cauthuList) {
        this.context = context;
        this.layout = layout;
        this.cauthuList = cauthuList;
    }

    @Override
    public int getCount() {
        return cauthuList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        //view chứa layout, layout nào hiển thị mỗi dòng
        view=inflater.inflate(layout,null);
        //CircleImageView CimgChandung=(CircleImageView) view.findViewById(R.id.CircleImageView);
        ImageView imgChandung=(ImageView) view.findViewById(R.id.imageviewHinh);
        ImageView imgQuocgia=(ImageView) view.findViewById(R.id.imageviewQuocgia);
        TextView txtTen=(TextView) view.findViewById(R.id.textviewTen);
        TextView txtNamsinh=(TextView) view.findViewById(R.id.textviewnamsinh);
        Cauthu cauthu=cauthuList.get(i);
        txtTen.setText(cauthu.getTen());
        txtNamsinh.setText(cauthu.getNamsinh());
        imgChandung.setImageResource(cauthu.getChandung());
        imgQuocgia.setImageResource(cauthu.getQuocqua());
        return view;

    }
}
