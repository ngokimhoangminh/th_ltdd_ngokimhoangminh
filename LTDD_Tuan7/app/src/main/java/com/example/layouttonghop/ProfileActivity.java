package com.example.layouttonghop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ProfileActivity extends Activity {
    ImageView imgCustom,imgProfile,imgLogout;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        imgLogout=(ImageView) findViewById(R.id.logout);
        imgProfile=(ImageView) findViewById(R.id.profile);
        imgCustom=(ImageView) findViewById(R.id.customlist);
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(ProfileActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ProfileActivity.this,"Bạn đã ở Profile",Toast.LENGTH_LONG).show();
            }
        });
        imgCustom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(ProfileActivity.this, CustomListActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
