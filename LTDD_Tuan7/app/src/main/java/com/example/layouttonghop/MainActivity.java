package com.example.layouttonghop;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
//import com.google.android.material.textfield.TextInputLayout;

//AppCompatActivity
public class MainActivity extends Activity {
    Button btnLogin;
    ImageView imgEye;
    //final TextInputLayout usernameWrapper = findViewById(R.id.usernameWrapper);
    TextInputEditText edtUser,edtpass;
    TextView txtSignUp;
    int status=1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnLogin =(Button) findViewById(R.id.login);
        edtUser =(TextInputEditText) findViewById(R.id.username);
        edtpass=(TextInputEditText) findViewById(R.id.password);
        txtSignUp=(TextView) findViewById(R.id.textviewSignUp);

        /*edtUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() ==0) {
                    usernameWrapper.setError("Bạn bắt buộc phải nhập username");
                } else {
                    usernameWrapper.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edtUser.getText().toString().equals("minh") && edtpass.getText().toString().equals("123")) {
                    Intent myIntent=new Intent(MainActivity.this, LoadingActivity.class);
                    startActivity(myIntent);
                    Toast.makeText(MainActivity.this,"Đăng nhập thành công",Toast.LENGTH_LONG).show();
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Đăng nhập thất bại",Toast.LENGTH_LONG).show();
                }
            }
        });
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent=new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(myIntent);
            }
        });
    }

}