package com.example.coffeehouse;

import org.w3c.dom.Text;

import java.security.PrivateKey;

public class TinTuc {
    private String title;
    private String hinh;
    private String hinhthuc;
    private String description;

    public TinTuc() {
    }

    public TinTuc(String title, String hinh, String hinhthuc, String description) {
        this.title = title;
        this.hinh = hinh;
        this.hinhthuc = hinhthuc;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHinh() {
        return hinh;
    }

    public void setHinh(String hinh) {
        this.hinh = hinh;
    }

    public String getHinhthuc() {
        return hinhthuc;
    }

    public void setHinhthuc(String hinhthuc) {
        this.hinhthuc = hinhthuc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
