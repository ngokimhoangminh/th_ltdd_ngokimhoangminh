package com.example.coffeehouse;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.squareup.picasso.Picasso;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

public class LoginActivity extends AppCompatActivity {
    Spinner spQuocgia;
    private ArrayList<HinhAnh> arrayHinhanh;
    private CountryAdapter adapter;
    TextView txtBoqua,btnEmail,btnFacebook;
    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager=CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        spQuocgia=(Spinner) findViewById(R.id.SpinerQuocgia);
        btnEmail=(TextView) findViewById(R.id.dangnhapEmail);
        btnFacebook=(TextView) findViewById(R.id.dangnhapFacebook);

        initList();
        adapter=new CountryAdapter(this,arrayHinhanh);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spQuocgia.setAdapter(adapter);



        btnEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog();
            }
        });
    }


    private void Dialog()
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_email);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        final TextView btnHuy=(TextView) dialog.findViewById(R.id.buttonHuy);
        final TextView btnDangNhap=(TextView) dialog.findViewById(R.id.buttpnDangNhap);
        btnDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LoginActivity.this,LoginEmailActivity.class));
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        dialog.show();
    }
    private  void initList()
    {
        arrayHinhanh=new ArrayList<>();
        arrayHinhanh.add(new HinhAnh(R.drawable.argentina));
        arrayHinhanh.add(new HinhAnh(R.drawable.bodaonha));
    }
}