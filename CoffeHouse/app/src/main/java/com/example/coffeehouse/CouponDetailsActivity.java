package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;

public class CouponDetailsActivity extends AppCompatActivity {
    TextView txt_coupon_batdau_31,txt_coupon_ketthuc_31,txt_coupon_thongtin_31;
    ImageView img_close_coupon;
    MaterialButton btn_dathang_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_details);
        txt_coupon_batdau_31=(TextView) findViewById(R.id.text_coupon_batdau_bd);
        txt_coupon_ketthuc_31=(TextView) findViewById(R.id.text_coupon_ketthuc_kt);
        txt_coupon_thongtin_31=(TextView) findViewById(R.id.text_coupon_thongtin);
        img_close_coupon=(ImageView) findViewById(R.id.image_close_detail_coupn);
        btn_dathang_31=(MaterialButton) findViewById(R.id.button_detail_coupon_dathang);
        Bundle bundle=getIntent().getExtras();
        String begin=bundle.getString("begin");
        String finish=bundle.getString("finish");
        String info=bundle.getString("info");
        txt_coupon_batdau_31.setText(begin);
        txt_coupon_ketthuc_31.setText(finish);
        int diemDung=info.indexOf("-");
        int tong=0;
        for(int i=0;i<info.length();i++)
        {
            if(info.contains("-"))
            {
                tong++;
            }
        }
       // String doan1=info.substring(0,diemDung);
        //Toast.makeText(CouponDetailsActivity.this,tong+"",Toast.LENGTH_LONG).show();
        txt_coupon_thongtin_31.setText(info);
        img_close_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        btn_dathang_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CouponDetailsActivity.this,GiaoHangActivity.class));
            }
        });
    }
}