package com.example.coffeehouse;

public class HinhAnh {
    private int hinhanh;

    public HinhAnh(int hinhanh) {
        this.hinhanh = hinhanh;
    }

    public int getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(int hinhanh) {
        this.hinhanh = hinhanh;
    }
}
