package com.example.coffeehouse;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DoAnFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DoAnFragment extends Fragment {
    ArrayList<Product> arrayThucannhe,arrBanhNgot,arrBanhMan;
    ArrayList<GioHang> arrGioHang;
    ArrayList<String> arrkey;
    GirdviewAdapter adapter1,adapter2,adapter3;
    ExpandableHeightGridView gridViewThucAnNhe,gridViewBanhNgot,gridviewBanhMan;
    DatabaseReference mData1,mData2,mData3,mData;
    ImageView imgHeart,imgPlus,imgMinus;
    TextView txtSoluong,txtTongtien, dl_txtSoluong, dl_txtTongTien;
    RelativeLayout rexemgiohang;
    int total=0,tongtien=0,tongsoluong=0,tongsotien=0,key=0;
    ProgressBar pb;
    Intent myIntent;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public DoAnFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DoAnFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DoAnFragment newInstance(String param1, String param2) {
        DoAnFragment fragment = new DoAnFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_do_an, container, false);
        gridViewThucAnNhe=(ExpandableHeightGridView) view.findViewById(R.id.girdviewThucannhe);
        gridViewBanhNgot=(ExpandableHeightGridView) view.findViewById(R.id.girdviewBanhngot);
        gridviewBanhMan=(ExpandableHeightGridView) view.findViewById(R.id.girdviewBanhMan);
        arrayThucannhe=new ArrayList<Product>();
        arrBanhNgot=new ArrayList<Product>();
        arrBanhMan=new ArrayList<Product>();
        arrGioHang=new ArrayList<GioHang>();
        mData= FirebaseDatabase.getInstance().getReference();
        pb=(ProgressBar) view.findViewById(R.id.progress_bar);
        dl_txtSoluong=(TextView) view.findViewById(R.id.dl_textsoluong);
        dl_txtTongTien=(TextView) view.findViewById(R.id.dl_textTongTien);
        rexemgiohang=(RelativeLayout) view.findViewById(R.id.xemgiohang);
        LoadThucannhe();
        LoadBanhNgot();
        LoadBanhMan();
        LoadGioHang();
        ///
            rexemgiohang.setVisibility(View.GONE);
            rexemgiohang.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   startActivity(new Intent(getContext(),GioHangActivity.class));
                }
            });
        ///
        //xu ly click
        gridViewThucAnNhe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Dialog(arrayThucannhe,i);
            }
        });
        gridViewBanhNgot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Dialog(arrBanhNgot,i);
            }
        });
        gridviewBanhMan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Dialog(arrBanhMan,i);
            }
        });
        return view;
    }
    private void LoadThucannhe()
    {
        mData1= FirebaseDatabase.getInstance().getReference().child("thucannhe");
        mData1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot1 : snapshot.getChildren())
                {
                    Product p=dataSnapshot1.getValue(Product.class);
                    arrayThucannhe.add(p);
                }
                adapter1=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrayThucannhe);
                gridViewThucAnNhe.setAdapter(adapter1);
                gridViewThucAnNhe.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadBanhNgot()
    {
        mData2= FirebaseDatabase.getInstance().getReference().child("banhngot");
        mData2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot2 : snapshot.getChildren())
                {
                    Product p=dataSnapshot2.getValue(Product.class);
                    arrBanhNgot.add(p);
                }
                adapter2=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrBanhNgot);
                gridViewBanhNgot.setAdapter(adapter2);
                gridViewBanhNgot.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadBanhMan()
    {
        mData3= FirebaseDatabase.getInstance().getReference().child("banhman");
        mData3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot3 : snapshot.getChildren())
                {
                    Product p=dataSnapshot3.getValue(Product.class);
                    arrBanhMan.add(p);
                }
                adapter3=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrBanhMan);
                gridviewBanhMan.setAdapter(adapter3);
                gridviewBanhMan.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void  LoadGioHang()
    {
        mData.child("GioHang").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
               GioHang gh=snapshot.getValue(GioHang.class);
                arrGioHang.add(new GioHang(gh.getTen(),gh.getSize(),gh.getMieuta(),gh.getHinhanh(),gh.getKey(),gh.getSoluong(),gh.getGiatien(),gh.isPermission()));
                rexemgiohang.setVisibility(View.VISIBLE);
                tongsoluong=0;
                tongsotien=0;
                for (int i=0;i<arrGioHang.size();i++){
                    tongsotien+=arrGioHang.get(i).getGiatien();
                    tongsoluong+=arrGioHang.get(i).getSoluong();
                    int location=i;
                }
                DecimalFormat format=new DecimalFormat("###,###.###");
                dl_txtSoluong.setText(tongsoluong+"");
                dl_txtTongTien.setText(format.format(tongsotien));

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private void Dialog(ArrayList<Product> mang,int vitri)
    {
        final Dialog dialog=new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_dathang_2);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        final TextView txtMieuta=(TextView) dialog.findViewById(R.id.dl_textmota);
        final TextView txtTen=(TextView) dialog.findViewById(R.id.dl_textTen);
        final TextView txtGia=(TextView) dialog.findViewById(R.id.dl_textGia);
        final ImageView imghinh=(ImageView) dialog.findViewById(R.id.dl_imagehinh);
        final TextView  txtTongtien=(TextView) dialog.findViewById(R.id.textTongtien);
        imgHeart=(ImageView) dialog.findViewById(R.id.imageHeart);
        imgMinus=(ImageView) dialog.findViewById(R.id.imageMinus);
        imgPlus=(ImageView) dialog.findViewById(R.id.imagePlus);
        txtSoluong=(TextView) dialog.findViewById(R.id.textSoluong);
        final LinearLayout lnTongTien=(LinearLayout) dialog.findViewById(R.id.dl_linearTongTien);

        txtMieuta.setText(mang.get(vitri).getDescrition());
        txtTen.setText(mang.get(vitri).getName());
        DecimalFormat format=new DecimalFormat("###,###.###");
        txtGia.setText(format.format(mang.get(vitri).getMoney()));
        txtTongtien.setText(txtGia.getText());
        Picasso.get().load(mang.get(vitri).getImage())
                .placeholder(R.drawable.tra1)
                .error(R.drawable.tra2)
                .into(imghinh);
        //imghinh.set(arrayPhobien.get(vitri).getImage());
        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMinus.setImageResource(R.drawable.ic_minus);
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                soluong++;
                txtSoluong.setText(soluong+"");
                int gia=mang.get(vitri).getMoney();
                total=0;
                total=gia*soluong;
                txtTongtien.setText(format.format(total) );
            }
        });
        imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                if(soluong>1)
                {
                    soluong--;
                    txtSoluong.setText(soluong+"");
                    int gia=mang.get(vitri).getMoney();
                    int total=0;
                    total=gia*soluong;
                    txtTongtien.setText(format.format(total));
                }
                if(soluong==1)
                {
                       imgMinus.setImageResource(R.drawable.minuss);
                }

            }
        });
        imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yeuthich();
            }
        });
        key++;
        lnTongTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ten=txtTen.getText().toString();
                String mieuta=txtMieuta.getText().toString();
                String link=mang.get(vitri).getImage();
                int soluong=Integer.parseInt(txtSoluong.getText().toString());

                if(soluong>1)
                {
                    tongtien=total;
                }else
                {
                    tongtien=mang.get(vitri).getMoney();
                }

                String tenkey="giohangdoan"+key;
                GioHang giohang=new GioHang(ten,"Vừa",mieuta,link,tenkey,soluong,tongtien,true);
                mData.child("GioHang").child(tenkey).setValue(giohang);
                //String key=mData.child("GioHang").getKey();
                //arrkey=new ArrayList<>();
                //arrkey.add(key);




                /////////


                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getContext(),ten+" đã được thêm vào giỏ hàng" ,Toast.LENGTH_LONG).show();
                       displayMessage(ten+" đã được thêm vào giỏ hàng");
                        tongsoluong=0;
                        tongsotien=0;
                        for (int i=0;i<arrGioHang.size();i++){
                            tongsotien+=arrGioHang.get(i).getGiatien();
                            tongsoluong+=arrGioHang.get(i).getSoluong();
                        }
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        dl_txtSoluong.setText(tongsoluong+"");
                        dl_txtTongTien.setText(format.format(tongsotien));
                        dialog.dismiss();
                    }
                },1000);
            }
        });
        int soluong=Integer.parseInt(txtSoluong.getText().toString());
        if (soluong==0)
        {
            lnTongTien.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
        dialog.show();
    }
    private  void displayMessage(String message)
    {
        Snackbar.make(rexemgiohang,message,Snackbar.LENGTH_SHORT).show();
    }
    int status=1;
    private  void yeuthich()
    {

        if(status==1)
        {
            status=2;
            imgHeart.setImageResource(R.drawable.heart2);
        }else
        {
            status=1;
            imgHeart.setImageResource(R.drawable.ic_heart);
        }
    }
}