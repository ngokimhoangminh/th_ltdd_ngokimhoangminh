package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ThanhToanActivity extends AppCompatActivity {
    LinearLayout ln1,ln2,ln3,ln4;
    ImageView cm1,cm2,cm3,cm4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thanh_toan);
        ln1=(LinearLayout) findViewById(R.id.linear1);
        ln2=(LinearLayout) findViewById(R.id.linear2);
        ln3=(LinearLayout) findViewById(R.id.linear3);
        ln4=(LinearLayout) findViewById(R.id.linear4);
        cm1=(ImageView) findViewById(R.id.checkmark1);
        cm2=(ImageView) findViewById(R.id.checkmark2);
        cm3=(ImageView) findViewById(R.id.checkmark3);
        cm4=(ImageView) findViewById(R.id.checkmark4);
        ln1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cm2.setVisibility(View.GONE);
                cm1.setVisibility(View.VISIBLE);
                cm3.setVisibility(View.GONE);
                cm4.setVisibility(View.GONE);
            }
        });
        ln2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cm2.setVisibility(View.VISIBLE);
                cm1.setVisibility(View.GONE);
                cm3.setVisibility(View.GONE);
                cm4.setVisibility(View.GONE);
            }
        });
        ln3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cm2.setVisibility(View.GONE);
                cm1.setVisibility(View.GONE);
                cm3.setVisibility(View.VISIBLE);
                cm4.setVisibility(View.GONE);
            }
        });
        ln4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cm2.setVisibility(View.GONE);
                cm1.setVisibility(View.GONE);
                cm3.setVisibility(View.GONE);
                cm4.setVisibility(View.VISIBLE);
            }
        });
    }
}