package com.example.coffeehouse;

public class GioHang {
    public String ten;
    private String size;
    public String mieuta;
    public String hinhanh;
    public String key;
    public Integer soluong;
    public Integer giatien;
    public boolean permission;

    public GioHang() {
    }

    public GioHang(String ten, String size, String mieuta, String hinhanh, String key, Integer soluong, Integer giatien, boolean permission) {
        this.ten = ten;
        this.size = size;
        this.mieuta = mieuta;
        this.hinhanh = hinhanh;
        this.key = key;
        this.soluong = soluong;
        this.giatien = giatien;
        this.permission = permission;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getMieuta() {
        return mieuta;
    }

    public void setMieuta(String mieuta) {
        this.mieuta = mieuta;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getSoluong() {
        return soluong;
    }

    public void setSoluong(Integer soluong) {
        this.soluong = soluong;
    }

    public Integer getGiatien() {
        return giatien;
    }

    public void setGiatien(Integer giatien) {
        this.giatien = giatien;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }
}
