package com.example.coffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CountryAdapter extends ArrayAdapter<HinhAnh> {
    public CountryAdapter(@NonNull Context context, ArrayList<HinhAnh> hinhAnhs) {
        super(context, 0,hinhAnhs);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView,parent);
    }
    private View initView(int position, View convertView, ViewGroup parent){
        if(convertView==null)
        {
            convertView= LayoutInflater.from(getContext()).inflate(
                    R.layout.spiner_layout,parent,false
            );
        }
        ImageView imgFlag=(ImageView) convertView.findViewById(R.id.hinhQuocgia);
        HinhAnh hinhAnh=getItem(position);
        if(convertView!=null)
        {
            imgFlag.setImageResource(hinhAnh.getHinhanh());
        }

        return convertView;
    }
}
