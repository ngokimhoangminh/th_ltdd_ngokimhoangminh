package com.example.coffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class GioHangAdapter extends BaseAdapter {
    private Context context;
    int layout;
    List<GioHang> giohangList;


    public GioHangAdapter(Context context, int layout, List<GioHang> giohangList) {
        this.context = context;
        this.layout = layout;
        this.giohangList = giohangList;

    }

    @Override
    public int getCount() {
        return giohangList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);

        TextView txtTen_31=(TextView) view.findViewById(R.id.text_giohang_ten);
        TextView txtGia_31=(TextView) view.findViewById(R.id.text_giohang_gia);
        TextView txtSize_31=(TextView) view.findViewById(R.id.text_giohang_size);
        TextView txtSoluong_31=(TextView) view.findViewById(R.id.text_giohang_soluong);
        TextView txtGia_size_31=(TextView) view.findViewById(R.id.text_giohang_gia_size);
        TextView txtMieuta_31=(TextView) view.findViewById(R.id.text_gh_mieuta);
        ImageView imgHinh_31=(ImageView) view.findViewById(R.id.image_gh);
        GioHang gioHang=giohangList.get(i);
        DatabaseReference mData;
       txtTen_31.setText(gioHang.getTen());
        // Locale localeVN = new Locale("vi", "VN");
        //NumberFormat currencyVN = NumberFormat.getCurrencyInstance(localeVN);
        //String tien= currencyVN.format(product.getMoney());
        DecimalFormat format=new DecimalFormat("###,###.###");
        txtGia_31.setText(format.format(gioHang.getGiatien())+" đ");
        txtSize_31.setText(gioHang.getSize());
        txtSoluong_31.setText(gioHang.getSoluong()+"");
        txtMieuta_31.setText(gioHang.getMieuta());
        Picasso.get().load(gioHang.getHinhanh()).into(imgHinh_31);
        txtGia_size_31.setText(format.format(gioHang.getGiatien())+" đ");
        return view;
    }
}

