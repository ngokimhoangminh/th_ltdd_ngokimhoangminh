package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class GioHangActivity<Module> extends AppCompatActivity {
    DatabaseReference mData_gio_hang,ref;
    ArrayList<GioHang> arrGioHang;
    GioHangAdapter gioHangAdapter;
    ExpandableHeightListView lv_giohang;
    FirebaseAuth firebaseAuth;
    EditText edtTen;
    int tongcong=0;
    TextView txtTamtinh,txtTongcong,txtTienmat,txtTienDathang;
    ImageView imgBack_31;
    TextView txtSoluong,txtTongtien,dl_txtSoluong, dl_txtTongTien;
    int total=0,tongsotien=0,soluong=0,giatien=0;
    ImageView imgHeart,imgPlus,imgMinus;
    CheckBox cb1,cb2,cb3;
    LinearLayout ln_mauudau_31,ln_tienmat_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gio_hang);
        Anhxa();
        mData_gio_hang= FirebaseDatabase.getInstance().getReference();
        arrGioHang=new ArrayList<GioHang>();
        firebaseAuth=FirebaseAuth.getInstance();
        edtTen.setText(firebaseAuth.getCurrentUser().getDisplayName());
        imgBack_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Load_GioHang();
        lv_giohang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DialogGioHang(i);
            }
        });
        ln_mauudau_31=(LinearLayout) findViewById(R.id.linear_mauudai);
        ln_tienmat_31=(LinearLayout) findViewById(R.id.linear_tienmat);
        ln_mauudau_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog_Mauudai();
            }
        });
        ln_tienmat_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GioHangActivity.this,ThanhToanActivity.class));
            }
        });

    }
    private void Dialog_Mauudai()
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_mauudai);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        final ImageView imgClose=(ImageView) dialog.findViewById(R.id.image_close_mauudai);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    private void Load_GioHang()
    {
        mData_gio_hang= FirebaseDatabase.getInstance().getReference().child("GioHang");
        mData_gio_hang.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    GioHang p=dataSnapshot.getValue(GioHang.class);
                   arrGioHang.add(p);
                   tongcong=0;
                   for(int i=0;i<arrGioHang.size();i++)
                   {
                       tongcong+=arrGioHang.get(i).getGiatien();
                   }
                    DecimalFormat format=new DecimalFormat("###,###.###");
                   txtTamtinh.setText(format.format(tongcong)+" đ");
                   txtTongcong.setText(format.format(tongcong)+" đ");
                   txtTienmat.setText(format.format(tongcong)+ " đ");
                   txtTienDathang.setText(format.format(tongcong) + "đ");
                }

                gioHangAdapter=new GioHangAdapter(GioHangActivity.this,R.layout.giohang_item,arrGioHang);
                lv_giohang.setAdapter(gioHangAdapter);
                lv_giohang.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(GioHangActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void DialogGioHang(int vitri)
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_dathang);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        //
        RelativeLayout reTieudesize=(RelativeLayout) dialog.findViewById(R.id.tieudesize);
        LinearLayout lnKhungsize=(LinearLayout) dialog.findViewById(R.id.khungsize);
        //
        final TextView txtMieuta=(TextView) dialog.findViewById(R.id.dl_textmota);
        final TextView txtTen=(TextView) dialog.findViewById(R.id.dl_textTen);
        final TextView txtGia=(TextView) dialog.findViewById(R.id.dl_textGia);
        final ImageView imghinh=(ImageView) dialog.findViewById(R.id.dl_imagehinh);
        imgHeart=(ImageView) dialog.findViewById(R.id.imageHeart);
        txtTongtien=(TextView) dialog.findViewById(R.id.textviewTongtien);
        imgMinus=(ImageView) dialog.findViewById(R.id.imageViewMinus);
        imgPlus=(ImageView) dialog.findViewById(R.id.imageviewPlus);
        txtSoluong=(TextView) dialog.findViewById(R.id.textviewSoluong);
        cb1=(CheckBox) dialog.findViewById(R.id.checkbox1);
        cb2=(CheckBox) dialog.findViewById(R.id.checkbox2);
        cb3=(CheckBox) dialog.findViewById(R.id.checkbox3);
        final LinearLayout lnTongTien=(LinearLayout) dialog.findViewById(R.id.dl_linearTongTien);
        tongsotien=arrGioHang.get(vitri).getGiatien();
        soluong=arrGioHang.get(vitri).getSoluong();
        if(arrGioHang.get(vitri).isPermission())
        {
            reTieudesize.setVisibility(View.GONE);
            lnKhungsize.setVisibility(View.GONE);
            giatien=tongsotien/soluong;
        }else{
            cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb2.setChecked(false);
                    cb3.setChecked(false);
                }
            });
            cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb1.setChecked(false);
                    cb3.setChecked(false);
                }
            });
            cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb2.setChecked(false);
                    cb1.setChecked(false);
                }
            });
            if(arrGioHang.get(vitri).getSize().equals("Nhỏ"))
            {
                cb1.setChecked(true);
                giatien=tongsotien/soluong;
            }
            if(arrGioHang.get(vitri).getSize().equals("Vừa"))
            {
                cb2.setChecked(true);
                giatien=(tongsotien-7000)/soluong;
            }
            if(arrGioHang.get(vitri).getSize().equals("Lớn"))
            {
                cb3.setChecked(true);
                giatien=(tongsotien-14000)/soluong;
            }
        }

        /////////////////////////////////////////////
        txtMieuta.setText(arrGioHang.get(vitri).getMieuta());
        txtTen.setText(arrGioHang.get(vitri).getTen());
        DecimalFormat format=new DecimalFormat("###,###.###");
        txtTongtien.setText(format.format(arrGioHang.get(vitri).getGiatien())+" đ");
        txtGia.setText(format.format(giatien));
        Picasso.get().load(arrGioHang.get(vitri).getHinhanh())
                .placeholder(R.drawable.tra1)
                .error(R.drawable.tra2)
                .into(imghinh);
        txtSoluong.setText(arrGioHang.get(vitri).getSoluong()+"");

        imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yeuthich();
            }
        });
        imgMinus.setImageResource(R.drawable.ic_minus);
        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMinus.setImageResource(R.drawable.ic_minus);
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                soluong++;
                txtSoluong.setText(soluong+"");
                tongsotien=tongsotien+giatien;
                txtTongtien.setText(format.format(tongsotien)+" đ");
            }
        });

        imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                soluong=Integer.parseInt(txtSoluong.getText().toString());
                if(soluong>0)
                {
                    soluong--;
                    txtSoluong.setText(soluong+"");
                    tongsotien-=giatien;
                    txtTongtien.setText(format.format(tongsotien)+" đ");
                }
                if(soluong==0)
                {
                        imgMinus.setImageResource(R.drawable.minuss);
                        txtTongtien.setText("Bỏ chọn món");
                        Disable();
                        lnTongTien.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String key=arrGioHang.get(vitri).getKey().toString();


                            xacnhanXoa(key);
                            dialog.dismiss();

                        }
                    });
                }


            }
        });

        lnTongTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key=arrGioHang.get(vitri).getKey().toString();
                ref= FirebaseDatabase.getInstance().getReference().child("GioHang").child(key);
                ref.child("giatien").setValue(tongsotien);
                ref.child("soluong").setValue(Integer.parseInt(txtSoluong.getText().toString()));
                dialog.dismiss();
            }
        });


        dialog.show();
    }
    private void xacnhanXoa(String key)
    {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Thông Báo!");
        alertDialog.setMessage("Bạn cần chọn ít nhất 1 món trong giỏ hàng");
        alertDialog.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ref= FirebaseDatabase.getInstance().getReference().child("GioHang").child(key);
                        //Toast.makeText(getContext(),ten+" đã được thêm vào giỏ hàng" ,Toast.LENGTH_LONG).show();
                        ref.removeValue();
                    }

        });
        alertDialog.setNegativeButton("Hủy", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.show();
    }
    private  void Disable(){
       cb1.setChecked(false);
       cb2.setChecked(false);
       cb3.setChecked(false);
    }
    int status=1;
    private  void yeuthich()
    {

        if(status==1)
        {
            status=2;
            imgHeart.setImageResource(R.drawable.heart2);
        }else
        {
            status=1;
            imgHeart.setImageResource(R.drawable.ic_heart);
        }
    }
    private  void Anhxa()
    {
        imgBack_31=(ImageView) findViewById(R.id.imageBackgiohang_31);
        edtTen=(EditText) findViewById(R.id.edittext_ten);
        lv_giohang=(ExpandableHeightListView) findViewById(R.id.listview_giohang);
        txtTamtinh=(TextView) findViewById(R.id.text_tamtinh);
        txtTongcong=(TextView) findViewById(R.id.text_tongcong_giohang);
        txtTienmat=(TextView) findViewById(R.id.text_tienmat);
        txtTienDathang=(TextView) findViewById(R.id.text_tien_dathang);
    }
}