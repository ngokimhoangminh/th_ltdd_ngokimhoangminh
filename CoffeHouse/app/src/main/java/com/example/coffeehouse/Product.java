package com.example.coffeehouse;

public class Product {
    private String image;
    private String name;
    private int money;
    private String descrition;
    private boolean permission;

    public Product() {
    }

    public Product(String image, String name, int money, String descrition, boolean permission) {
        this.image = image;
        this.name = name;
        this.money = money;
        this.descrition = descrition;
        this.permission = permission;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public String getDescrition() {
        return descrition;
    }

    public void setDescrition(String descrition) {
        this.descrition = descrition;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }
}
