package com.example.coffeehouse;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class OnBoardAdapter extends RecyclerView.Adapter<OnBoardAdapter.OnboardingViewHolder>{
    private List<OnBoardItem> onBoardItems;

    public OnBoardAdapter(List<OnBoardItem> onBoardItems) {
        this.onBoardItems = onBoardItems;
    }

    @NonNull
    @Override
    public OnboardingViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OnboardingViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(
                        R.layout.item_container_onboarding,parent,false
                )
        );
    }

    @Override
    public void onBindViewHolder(@NonNull OnboardingViewHolder holder, int position) {
        holder.setOnboardingData(onBoardItems.get(position));
    }

    @Override
    public int getItemCount() {
        return onBoardItems.size();
    }

    class OnboardingViewHolder extends RecyclerView.ViewHolder{
        private TextView txtTitle;
        private TextView txtDescription;
        private ImageView imgOnboarding;

        OnboardingViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTitle=(TextView) itemView.findViewById(R.id.textTitle);
            txtDescription=(TextView) itemView.findViewById(R.id.textDescription);
            imgOnboarding=(ImageView) itemView.findViewById(R.id.imageOnboaring);
        }
        void setOnboardingData(OnBoardItem onBoardItem)
        {
            txtTitle.setText(onBoardItem.getTitle());
            txtDescription.setText(onBoardItem.getDescription());
            imgOnboarding.setImageResource(onBoardItem.getImage());
        }

    }
}
