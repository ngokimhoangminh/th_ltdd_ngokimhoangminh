package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;

public class TaiKhoanActivity extends AppCompatActivity {
    BottomNavigationView navigationView;
    LinearLayout lnGiupDo,lnNhac,lnThongtintaikhoan_31,lnLichsu_31,ln_Caidat_31;
    FirebaseAuth firebaseAuth;
    TextView txtTenDangNhap,txtDangxuat_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tai_khoan);
        firebaseAuth=FirebaseAuth.getInstance();
        lnGiupDo=(LinearLayout) findViewById(R.id.lnGiupDo);
        lnNhac=(LinearLayout) findViewById(R.id.linearlayoutNhac);
        lnThongtintaikhoan_31=(LinearLayout) findViewById(R.id.linearlayout_thongtintaikhoan_31);
        lnLichsu_31=(LinearLayout) findViewById(R.id.linearlayoutLichsu_31);
        ln_Caidat_31=(LinearLayout) findViewById(R.id.linearlayout_caidat_31);
        txtDangxuat_31=(TextView) findViewById(R.id.textdangxuat_31);
        txtDangxuat_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                firebaseAuth.signOut();
                startActivity(new Intent(TaiKhoanActivity.this,LoginActivity.class));
            }
        });
        ln_Caidat_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TaiKhoanActivity.this,CaiDatActivity.class));
            }
        });
        txtTenDangNhap=(TextView) findViewById(R.id.textTenDangNhap);

        txtTenDangNhap.setText(firebaseAuth.getCurrentUser().getDisplayName());
        lnLichsu_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TaiKhoanActivity.this,LichSuDonHangActivity.class));
            }
        });
        lnThongtintaikhoan_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TaiKhoanActivity.this,ThongTinTaiKhoanActivity.class));
            }
        });
        lnNhac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TaiKhoanActivity.this,MusicActivity.class));
            }
        });
        lnGiupDo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TaiKhoanActivity.this,GiupDoActivity.class));
            }
        });
        navigationView=(BottomNavigationView) findViewById(R.id.navigation);
        navigationView.setSelectedItemId(R.id.navigation_taikhoan);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch ((item.getItemId()))
                {
                    case R.id.navigation_taikhoan:
                        return true;
                    case R.id.navigation_tintuc:
                        Intent intent=new Intent(getApplicationContext(),TinTucActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_shop:
                        Intent intent2=new Intent(getApplicationContext(),CuaHangActivity.class);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_dathang:
                        Intent intent3=new Intent(getApplicationContext(),GiaoHangActivity.class);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });
    }
}