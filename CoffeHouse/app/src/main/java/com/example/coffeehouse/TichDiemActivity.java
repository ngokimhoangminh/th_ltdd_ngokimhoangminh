package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TichDiemActivity extends AppCompatActivity {
    ListView lv_tichdiem;
    ArrayList<Coupon> arrTichDiem;
    CouponAdapter adapter;
    DatabaseReference mData;
    ImageView img_close_tichdiem_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tich_diem);
        arrTichDiem=new ArrayList<Coupon>();
        lv_tichdiem=(ListView) findViewById(R.id.listview_tichdiem);
        LoadTichdiem();
        lv_tichdiem.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(TichDiemActivity.this,CouponDetailsActivity.class);
                intent.putExtra("begin",arrTichDiem.get(i).getBatdau());
                intent.putExtra("finish",arrTichDiem.get(i).getKetthuc());
                intent.putExtra("info",arrTichDiem.get(i).getThongtin());
                startActivity(intent);
            }
        });
        img_close_tichdiem_31=(ImageView) findViewById(R.id.image_close_tichdiem);
        img_close_tichdiem_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TichDiemActivity.this,TinTucActivity.class));
            }
        });
    }
    private void LoadTichdiem()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("Coupon");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    Coupon p=dataSnapshot.getValue(Coupon.class);
                    arrTichDiem.add(p);
                }
                adapter=new CouponAdapter(TichDiemActivity.this,R.layout.coupon_item,arrTichDiem);
                lv_tichdiem.setAdapter(adapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(TichDiemActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
}