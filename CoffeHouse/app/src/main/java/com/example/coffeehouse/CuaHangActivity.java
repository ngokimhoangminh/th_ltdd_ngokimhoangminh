package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class CuaHangActivity extends AppCompatActivity implements OnMapReadyCallback {
    MapFragment mapFragment;
    GoogleMap map;
    BottomNavigationView navigationView;
    Spinner spThanhPho;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cua_hang);

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        navigationView=(BottomNavigationView) findViewById(R.id.navigation);
        navigationView.setSelectedItemId(R.id.navigation_shop);
        spThanhPho=(Spinner) findViewById(R.id.spinerThanhpho);
        final ArrayList<String> arrThanhPho=new ArrayList<>();
        arrThanhPho.add("Hà Nội");
        arrThanhPho.add("Cần Thơ");
        arrThanhPho.add("TP Hồ Chí Minh");
        arrThanhPho.add("Bình Dương");
        arrThanhPho.add("Bạc Liêu");
        arrThanhPho.add("TP Huế");
        arrThanhPho.add("Quảng Nam");
        arrThanhPho.add("Quảng Bình");
        arrThanhPho.add("Quảng Ninh");
        ArrayAdapter adapter=new ArrayAdapter(this, android.R.layout.simple_spinner_item,arrThanhPho);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spThanhPho.setAdapter(adapter);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch ((item.getItemId()))
                {
                    case R.id.navigation_dathang:
                        Intent intent=new Intent(getApplicationContext(),GiaoHangActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_tintuc:
                        Intent intent2=new Intent(getApplicationContext(),TinTucActivity.class);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_shop:
                        return true;
                    case  R.id.navigation_taikhoan:
                        Intent intent3=new Intent(getApplicationContext(),TaiKhoanActivity.class);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        LatLng cf1 = new LatLng(16.0680418, 108.2094249);
        LatLng cf2 = new LatLng(16.0697527, 108.2151522);
        LatLng cf3 = new LatLng(16.0546059, 108.2182103);
        LatLng cf4 = new LatLng(16.0709787, 108.2190851);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(cf1, 14));
        map.addMarker(new MarkerOptions().title("The Coffee House").snippet("435 Lê Duẫn, Quận Thanh Khê").position(cf1));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(cf2, 14));
        map.addMarker(new MarkerOptions().title("The Coffee House").snippet("80 Paster, Quận Hải Châu").position(cf2));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(cf3, 14));
        map.addMarker(new MarkerOptions().title("The Coffee House").snippet("1 Núi Thành, Quận Hải Châu").position(cf3));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(cf4, 14));
        map.addMarker(new MarkerOptions().title("The Coffee House").snippet("80A Nguyễn Chí Thanh, Quận Hải Châu").position(cf4));
    }
}