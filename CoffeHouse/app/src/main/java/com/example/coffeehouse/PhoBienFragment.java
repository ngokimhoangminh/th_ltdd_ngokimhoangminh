package com.example.coffeehouse;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PhoBienFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PhoBienFragment extends Fragment {
    ArrayList<Product> arrayPhobien;
    ArrayList<GioHang> arrGioHang;
    GirdviewAdapter adapter;
    GridView gridViewpb;
    DatabaseReference reference,mData_giohang_31;
    ImageView imgHeart,imgPlus,imgMinus, imghinh;
    TextView txtMieuta,txtTen,txtGia,txtSoluong,txtTongtien,dl_txtSoluong, dl_txtTongTien;
    RelativeLayout re_xemgiohang_phobien_31;
    int total=0,tongtien=0,tongsoluong=0,tongsotien=0,tienthem=0,key=0;
    String loai;
    CheckBox cb1,cb2,cb3;
    Boolean permission;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public PhoBienFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PhoBienFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PhoBienFragment newInstance(String param1, String param2) {
        PhoBienFragment fragment = new PhoBienFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_pho_bien, container, false);
        gridViewpb=(GridView) view.findViewById(R.id.girdviewPhobien);
        re_xemgiohang_phobien_31=(RelativeLayout) view.findViewById(R.id.xemgiohang_phobien);
        dl_txtSoluong=(TextView) view.findViewById(R.id.dl_textsoluong);
        dl_txtTongTien=(TextView) view.findViewById(R.id.dl_textTongTien);
        re_xemgiohang_phobien_31.setVisibility(View.GONE);
        arrayPhobien=new ArrayList<Product>();
        mData_giohang_31= FirebaseDatabase.getInstance().getReference();
        arrGioHang=new ArrayList<GioHang>();
        reference= FirebaseDatabase.getInstance().getReference().child("phobiens");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot1 : snapshot.getChildren())
                {
                    Product p=dataSnapshot1.getValue(Product.class);
                    arrayPhobien.add(p);
                }
                adapter=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrayPhobien);
                gridViewpb.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
        re_xemgiohang_phobien_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(),GioHangActivity.class));
            }
        });
        LoadGioHang();
        //xư ly click
        gridViewpb.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                DialogDathang(i);

            }
        });
        return view;
    }
    private void  LoadGioHang()
    {
        mData_giohang_31.child("GioHang").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                GioHang gh=snapshot.getValue(GioHang.class);
                arrGioHang.add(new GioHang(gh.getTen(),gh.getSize(),gh.getMieuta(),gh.getHinhanh(),gh.getKey(),gh.getSoluong(),gh.getGiatien(),gh.isPermission()));
                re_xemgiohang_phobien_31.setVisibility(View.VISIBLE);
                tongsoluong=0;
                tongsotien=0;
                for (int i=0;i<arrGioHang.size();i++){
                    tongsotien+=arrGioHang.get(i).getGiatien();
                    tongsoluong+=arrGioHang.get(i).getSoluong();
                }
                DecimalFormat format=new DecimalFormat("###,###.###");
                dl_txtSoluong.setText(tongsoluong+"");
                dl_txtTongTien.setText(format.format(tongsotien));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private void DialogDathang(int vitri)
    {
        final Dialog dialog=new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_dathang);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        //
        RelativeLayout reTieudesize=(RelativeLayout) dialog.findViewById(R.id.tieudesize);
        LinearLayout lnKhungsize=(LinearLayout) dialog.findViewById(R.id.khungsize);
        //
        txtMieuta=(TextView) dialog.findViewById(R.id.dl_textmota);
        txtTen=(TextView) dialog.findViewById(R.id.dl_textTen);
        txtGia=(TextView) dialog.findViewById(R.id.dl_textGia);
        imghinh=(ImageView) dialog.findViewById(R.id.dl_imagehinh);
        imgHeart=(ImageView) dialog.findViewById(R.id.imageHeart);
        txtTongtien=(TextView) dialog.findViewById(R.id.textviewTongtien);
        imgMinus=(ImageView) dialog.findViewById(R.id.imageViewMinus);
        imgPlus=(ImageView) dialog.findViewById(R.id.imageviewPlus);
        txtSoluong=(TextView) dialog.findViewById(R.id.textviewSoluong);
        final TextView txt_loai_nho_31=(TextView) dialog.findViewById(R.id.text_loai_nho);
        final TextView txt_loai_vua_31=(TextView) dialog.findViewById(R.id.text_loai_vua);
        final TextView txt_loai_lon_31=(TextView) dialog.findViewById(R.id.text_loai_lon);
        cb1=(CheckBox) dialog.findViewById(R.id.checkbox1);
        cb2=(CheckBox) dialog.findViewById(R.id.checkbox2);
        cb3=(CheckBox) dialog.findViewById(R.id.checkbox3);
        cb1.setChecked(true);
        final LinearLayout lnTongTien=(LinearLayout) dialog.findViewById(R.id.dl_linearTongTien);
        cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cb2.setChecked(false);
                cb3.setChecked(false);
            }
        });
        cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cb1.setChecked(false);
                cb3.setChecked(false);
            }
        });
        cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                cb2.setChecked(false);
                cb1.setChecked(false);
            }
        });
        if(arrayPhobien.get(vitri).isPermission())
        {
            reTieudesize.setVisibility(View.GONE);
            lnKhungsize.setVisibility(View.GONE);
            permission=true;
        }else
        {
            permission=false;
        }
        imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                yeuthich();
            }
        });
        ////////////////////////////////////////////////////////////////
        txtMieuta.setText(arrayPhobien.get(vitri).getDescrition());
        txtTen.setText(arrayPhobien.get(vitri).getName());

        DecimalFormat format=new DecimalFormat("###,###.###");
        txtGia.setText(format.format(arrayPhobien.get(vitri).getMoney()));
        txtTongtien.setText(txtGia.getText()+" đ");
        Picasso.get().load(arrayPhobien.get(vitri).getImage())
                .placeholder(R.drawable.tra1)
                .error(R.drawable.tra2)
                .into(imghinh);
        key++;
        lnTongTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ten=arrayPhobien.get(vitri).getName();
                String mieuta=arrayPhobien.get(vitri).getDescrition();
                String link=arrayPhobien.get(vitri).getImage();
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                if(soluong>1)
                {
                    tongtien=total;
                }else
                {
                    tongtien=arrayPhobien.get(vitri).getMoney();
                }
                if(cb1.isChecked())
                {
                    loai=txt_loai_nho_31.getText().toString();
                }
                if (cb2.isChecked())
                {
                    loai=txt_loai_vua_31.getText().toString();
                    tienthem=7000*soluong;
                }
                if (cb3.isChecked())
                {
                    loai=txt_loai_lon_31.getText().toString();
                    tienthem=14000*soluong;
                }
                tongtien+=tienthem;
                String tenkey="giohangphobien"+key;
                GioHang giohang=new GioHang(ten,loai,mieuta,link,tenkey,soluong,tongtien,permission);
                mData_giohang_31.child("GioHang").child(tenkey).setValue(giohang);
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getContext(),ten+" đã được thêm vào giỏ hàng" ,Toast.LENGTH_LONG).show();
                        displayMessage(ten+" đã được đưa vào giỏ hàng");
                        tongsoluong=0;
                        tongsotien=0;
                        for (int i=0;i<arrGioHang.size();i++){
                            tongsotien+=arrGioHang.get(i).getGiatien();
                            tongsoluong+=arrGioHang.get(i).getSoluong();
                        }
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        dl_txtSoluong.setText(tongsoluong+"");
                        dl_txtTongTien.setText(format.format(tongsotien));
                        dialog.dismiss();
                    }
                },1000);

            }
        });
        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMinus.setImageResource(R.drawable.ic_minus);
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                soluong++;
                txtSoluong.setText(soluong+"");
                int gia=arrayPhobien.get(vitri).getMoney();
                total=0;
                total=gia*soluong;
                txtTongtien.setText(format.format(total)+" đ");
            }
        });
        imgMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                if(soluong>1)
                {
                    soluong--;
                    txtSoluong.setText(soluong+"");
                    int gia=arrayPhobien.get(vitri).getMoney();
                    int total=0;
                    total=gia*soluong;
                    txtTongtien.setText(format.format(total)+" đ");
                }

                    if(soluong==1)
                    {
                        imgMinus.setImageResource(R.drawable.minuss);
                    }



            }
        });
        //imghinh.set(arrayPhobien.get(vitri).getImage());
        dialog.show();
    }
    private  void displayMessage(String message)
    {
        Snackbar.make(re_xemgiohang_phobien_31,message,Snackbar.LENGTH_SHORT).show();
    }
    int status=1;
    private  void yeuthich()
    {

        if(status==1)
        {
            status=2;
            imgHeart.setImageResource(R.drawable.heart2);
        }else
        {
            status=1;
            imgHeart.setImageResource(R.drawable.ic_heart);
        }
    }


}