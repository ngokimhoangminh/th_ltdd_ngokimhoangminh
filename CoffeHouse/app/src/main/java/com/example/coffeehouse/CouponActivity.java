package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class CouponActivity extends AppCompatActivity {
    ArrayList<Coupon> arrCoupon;
    CouponAdapter adapter;
    ExpandableHeightListView lv_coupon_31;
    DatabaseReference mData;
    ImageView img_coupon_close_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon);
        arrCoupon=new ArrayList<Coupon>();
        img_coupon_close_31=(ImageView) findViewById(R.id.image_close_coupon);
        img_coupon_close_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(CouponActivity.this,TinTucActivity.class));
            }
        });
        lv_coupon_31=(ExpandableHeightListView) findViewById(R.id.list_coupon);
        LoadCoupon();
        lv_coupon_31.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(CouponActivity.this,CouponDetailsActivity.class);
                intent.putExtra("begin",arrCoupon.get(i).getBatdau());
                intent.putExtra("finish",arrCoupon.get(i).getKetthuc());
                intent.putExtra("info",arrCoupon.get(i).getThongtin());
                startActivity(intent);
            }
        });
    }
    private void LoadCoupon()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("Coupon");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    Coupon p=dataSnapshot.getValue(Coupon.class);
                    arrCoupon.add(p);
                }
                adapter=new CouponAdapter(CouponActivity.this,R.layout.coupon_item,arrCoupon);
                lv_coupon_31.setAdapter(adapter);
                lv_coupon_31.setExpanded(true);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(CouponActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
}