package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class LoginEmailActivity extends AppCompatActivity implements FirebaseAuth.AuthStateListener{
    ImageView imgBack;
    FirebaseAuth firebaseAuth;
    EditText edtEmail,edtMatKhau;
    MaterialButton btnTieptuc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_email);
        imgBack=(ImageView) findViewById(R.id.imageback);
        edtEmail=(EditText) findViewById(R.id.edittextEmail);
        edtMatKhau=(EditText) findViewById(R.id.editMatkhau);
        btnTieptuc=(MaterialButton) findViewById(R.id.buttonTieptuc);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        firebaseAuth=FirebaseAuth.getInstance();
        //Dangky();
        firebaseAuth.signOut();
        btnTieptuc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               Dangnhap();

            }
        });


    }
    private  void Dangky()
    {

       firebaseAuth.createUserWithEmailAndPassword("hoangminhcp10@gmail.com","123456")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            Toast.makeText(LoginEmailActivity.this, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(LoginEmailActivity.this, "Đăng ký thất bại", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    private void Dangnhap() {
        String email=edtEmail.getText().toString();
        String pass=edtMatKhau.getText().toString();
        FirebaseUser user=firebaseAuth.getCurrentUser();
        firebaseAuth.signInWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful())
                        {
                            Toast.makeText(LoginEmailActivity.this,"Đăng nhập thất bại",Toast.LENGTH_LONG).show();
                        }
                        // ...
                    }


                });
    }
    @Override
    protected void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(this);
    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user=firebaseAuth.getCurrentUser();
        if(user!=null)
        {

            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName("Hoàng Minh")
                    .setPhotoUri(Uri.parse("https://scontent-hkt1-1.xx.fbcdn.net/v/t1.0-9/120279693_783614828881623_8776313476938986715_o.jpg?_nc_cat=105&ccb=2&_nc_sid=09cbfe&_nc_ohc=nHhlVD2j1L4AX9B04U7&_nc_ht=scontent-hkt1-1.xx&oh=1c62124d394c64e330b43b8bbb3697a3&oe=5FE8D646"))
                    .build();
            user.updateProfile(profileUpdates);
            startActivity(new Intent(LoginEmailActivity.this,TinTucActivity.class));
        }

    }
}