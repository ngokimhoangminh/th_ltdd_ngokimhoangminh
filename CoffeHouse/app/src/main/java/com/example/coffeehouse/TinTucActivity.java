package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class TinTucActivity extends AppCompatActivity {
    ArrayList<TinTuc> arrayUudai ,arrayLover;
    ArrayList<TinTuc> arrayCapnhat;
    TinTucApdater adapter,adapter2,adapter3;
    RecyclerView re1,re2,re3;
    BottomNavigationView navigationView;
    DatabaseReference mData,mData2,mData3;
    FirebaseAuth firebaseAuth;
    TextView txtTenDangNhap;
    ImageView imgUs;
    LinearLayout ln_tichdiem_31,ln_giaohang_31,ln_coupon_31;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tin_tuc);
        Anhxa();

        arrayLover=new ArrayList<TinTuc>();
        arrayCapnhat=new ArrayList<TinTuc>();
        arrayUudai=new ArrayList<TinTuc>();

        LoadLover();
        LoadCapnhat();
        LoadUudai();
        //
        navigationView.setSelectedItemId(R.id.navigation_tintuc);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch ((item.getItemId()))
                {
                    case R.id.navigation_dathang:
                        Intent intent=new Intent(getApplicationContext(),GiaoHangActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_tintuc:
                        return true;
                    case R.id.navigation_shop:
                        Intent intent2=new Intent(getApplicationContext(),CuaHangActivity.class);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_taikhoan:
                        Intent intent3=new Intent(getApplicationContext(),TaiKhoanActivity.class);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        firebaseAuth=FirebaseAuth.getInstance();

        txtTenDangNhap.setText(firebaseAuth.getCurrentUser().getDisplayName());

        ln_giaohang_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TinTucActivity.this,GiaoHangActivity.class));
            }
        });
        ln_tichdiem_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TinTucActivity.this,CouponActivity.class));
            }
        });
        ln_coupon_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TinTucActivity.this,TichDiemActivity.class));
            }
        });
    }

    private void LoadUudai()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("uudai");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot1 : snapshot.getChildren())
                {
                    TinTuc p=dataSnapshot1.getValue(TinTuc.class);
                    arrayUudai.add(p);
                }
                LinearLayoutManager manager=new LinearLayoutManager(TinTucActivity.this);
                manager.setOrientation(LinearLayoutManager.HORIZONTAL);
                re1.setLayoutManager(manager);
                adapter=new TinTucApdater(TinTucActivity.this,arrayUudai);
                re1.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(TinTucActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadLover()
    {
        mData3= FirebaseDatabase.getInstance().getReference().child("lover");
        mData3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot2 : snapshot.getChildren())
                {
                    TinTuc p=dataSnapshot2.getValue(TinTuc.class);
                    arrayLover.add(p);
                }
                LinearLayoutManager manager3=new LinearLayoutManager(TinTucActivity.this);
                manager3.setOrientation(LinearLayoutManager.HORIZONTAL);
                re3.setLayoutManager(manager3);
                adapter3=new TinTucApdater(TinTucActivity.this,arrayLover);
                re3.setAdapter(adapter3);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(TinTucActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadCapnhat()
    {
        mData2= FirebaseDatabase.getInstance().getReference().child("capnhat");
        mData2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot1 : snapshot.getChildren())
                {
                    TinTuc p=dataSnapshot1.getValue(TinTuc.class);
                    arrayCapnhat.add(p);
                }
                LinearLayoutManager manager2=new LinearLayoutManager(TinTucActivity.this);
                manager2.setOrientation(LinearLayoutManager.HORIZONTAL);
                re2.setLayoutManager(manager2);
                adapter2=new TinTucApdater(TinTucActivity.this,arrayCapnhat);
                re2.setAdapter(adapter2);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(TinTucActivity.this,".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void Anhxa()
    {
        imgUs=(ImageView) findViewById(R.id.imageUs);
        txtTenDangNhap=(TextView) findViewById(R.id.TextviewTenDangnhap);
        re1=(RecyclerView) findViewById(R.id.recyclerview1);
        re2=(RecyclerView) findViewById(R.id.recyclerview2);
        re3=(RecyclerView) findViewById(R.id.recyclerview3);
        navigationView=(BottomNavigationView) findViewById(R.id.navigation);
        ln_giaohang_31=(LinearLayout) findViewById(R.id.linear_giaohang);
        ln_tichdiem_31=(LinearLayout) findViewById(R.id.linear_tichdiem);
        ln_coupon_31=(LinearLayout) findViewById(R.id.linear_coupon);
    }
}