package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

public class ThongTinTaiKhoanActivity extends AppCompatActivity {
    ImageView imgClose;
    TextView txtTen,txtEmail;
    FirebaseAuth firebaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thong_tin_tai_khoan);
        imgClose=(ImageView) findViewById(R.id.image_close);
        txtTen=(TextView) findViewById(R.id.text_tk_ten_31);
        txtEmail=(TextView) findViewById(R.id.text_tk_email_31);
        firebaseAuth=FirebaseAuth.getInstance();
        txtTen.setText(firebaseAuth.getCurrentUser().getDisplayName());
        txtEmail.setText(firebaseAuth.getCurrentUser().getEmail());
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}