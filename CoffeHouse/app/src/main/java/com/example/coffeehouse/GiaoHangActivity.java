package com.example.coffeehouse;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

public class GiaoHangActivity extends AppCompatActivity {
    BottomNavigationView navigationView;
    TabLayout mtablayout;
    ViewPager mviewpager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_giao_hang);
        Anhxa();
        navigationView.setSelectedItemId(R.id.navigation_dathang);
        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch ((item.getItemId()))
                {
                    case R.id.navigation_dathang:
                        return true;
                    case R.id.navigation_tintuc:
                        Intent intent=new Intent(getApplicationContext(),TinTucActivity.class);
                        startActivity(intent);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_shop:
                        Intent intent2=new Intent(getApplicationContext(),CuaHangActivity.class);
                        startActivity(intent2);
                        overridePendingTransition(0,0);
                        return true;
                    case R.id.navigation_taikhoan:
                        Intent intent3=new Intent(getApplicationContext(),TaiKhoanActivity.class);
                        startActivity(intent3);
                        overridePendingTransition(0,0);
                        return true;
                }
                return false;
            }
        });

        ViewPagerAdapter viewPageAdapter=new ViewPagerAdapter(getSupportFragmentManager(), FragmentStatePagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
        mviewpager.setAdapter(viewPageAdapter);
        mtablayout.setupWithViewPager(mviewpager);

        DialogShow();
    }

    private  void DialogShow()
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_giaohang);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        LinearLayout ln_tudenlay=(LinearLayout) dialog.findViewById(R.id.linear_tudenlay);
        ln_tudenlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(GiaoHangActivity.this,TuDenLayActivity.class));
            }
        });
        dialog.show();
    }
    private void Anhxa()
    {
        mtablayout=(TabLayout) findViewById(R.id.tabLayout);
        mviewpager=(ViewPager) findViewById(R.id.viewPager);
        navigationView=(BottomNavigationView) findViewById(R.id.navigation);
    }
}