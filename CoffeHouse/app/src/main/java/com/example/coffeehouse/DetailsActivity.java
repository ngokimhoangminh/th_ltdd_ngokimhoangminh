package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DetailsActivity extends AppCompatActivity {
    ImageView img_logo_31,img_details_close_31;
    TextView txt_des_31,txt_title_31;
    ArrayList<TinTuc> tinTucs;
    MaterialButton btn_order;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        img_logo_31=(ImageView) findViewById(R.id.image_detail);
        txt_des_31=(TextView) findViewById(R.id.textview_details);
        txt_title_31=(TextView) findViewById(R.id.text_title);
        btn_order=(MaterialButton) findViewById(R.id.button_order);
        img_details_close_31=(ImageView) findViewById(R.id.image_details_close);


        Bundle bundle=getIntent().getExtras();
        String text=bundle.getString("text");
        String logo=bundle.getString("hinh");
        String title=bundle.getString("title");
        String type=bundle.getString("type");
        txt_des_31.setText(text);
        txt_title_31.setText(title);
        tinTucs=new ArrayList<TinTuc>();
        Picasso.get().load(logo)
                .placeholder(R.drawable.tra1)
                .error(R.drawable.tra2)
                .into(img_logo_31);
        img_details_close_31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        if(type.equals("Order ngay"))
        {
            btn_order.setVisibility(View.VISIBLE);
            btn_order.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(DetailsActivity.this,GiaoHangActivity.class));
                }
            });
        }
    }
}