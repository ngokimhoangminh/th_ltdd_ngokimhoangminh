package com.example.coffeehouse;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class TinTucApdater extends RecyclerView.Adapter<TinTucApdater.MyViewHolder> {
    private TinTucActivity context;
    private List<TinTuc> tinTucList;

    public TinTucApdater(TinTucActivity context, List<TinTuc> tinTucList) {
        this.context = context;
        this.tinTucList = tinTucList;
    }

    public class MyViewHolder extends  RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        ImageView imgHinh;
        TextView txtTitle,txtDescription,txthinhthuc;
        RelativeLayout re_details;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgHinh=(ImageView) itemView.findViewById(R.id.imageHinh);
            txtTitle=(TextView) itemView.findViewById(R.id.textTitle);
            txtDescription=(TextView) itemView.findViewById(R.id.textViewDesciption);
            txthinhthuc=(TextView) itemView.findViewById(R.id.TextviewHinhthucr);
            re_details=(RelativeLayout) itemView.findViewById(R.id.relative_details);

        }


        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

        }
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.grid_tintuc_layout,parent,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        TinTuc tinTuc=tinTucList.get(position);
        holder.txtTitle.setText(tinTuc.getTitle());
       holder.txtDescription.setText(tinTuc.getDescription());
       holder.txthinhthuc.setText(tinTuc.getHinhthuc());
       Picasso.get().load(tinTuc.getHinh()).into(holder.imgHinh);
       holder.re_details.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Intent intent=new Intent(context,DetailsActivity.class);
               intent.putExtra("hinh",tinTuc.getHinh());
               intent.putExtra("text",tinTuc.getDescription());
               intent.putExtra("title",tinTuc.getTitle());
               intent.putExtra("type",holder.txthinhthuc.getText().toString());
               context.startActivity(intent);
           }
       });
       holder.txthinhthuc.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
              holder.txthinhthuc.setBackground(context.getResources().getDrawable(R.drawable.botron5));
              holder.txthinhthuc.setTextColor(context.getResources().getColor(R.color.white));
               if (holder.txthinhthuc.getText().equals("Order ngay"))
               {
                   Intent myintent=new Intent(context,GiaoHangActivity.class);
                   context.startActivity(myintent);
               }
               else {
                   Intent intent=new Intent(context,DetailsActivity.class);
                   intent.putExtra("hinh",tinTuc.getHinh());
                   intent.putExtra("text",tinTuc.getDescription());
                   intent.putExtra("title",tinTuc.getTitle());
                   intent.putExtra("type",tinTuc.getHinhthuc());
                   context.startActivity(intent);
               }
               Handler handler=new Handler();
               handler.postDelayed(new Runnable() {
                   @Override
                   public void run() {
                       holder.txthinhthuc.setBackground(context.getResources().getDrawable(R.drawable.botron3));
                       holder.txthinhthuc.setTextColor(context.getResources().getColor(R.color.orange));
                   }
               },1000);
           }
       });

    }

    @Override
    public int getItemCount() {
        return tinTucList.size();
    }
}
