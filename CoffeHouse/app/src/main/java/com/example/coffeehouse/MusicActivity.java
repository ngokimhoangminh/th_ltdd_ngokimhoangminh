package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import de.hdodenhof.circleimageview.CircleImageView;

public class MusicActivity extends AppCompatActivity {
    ImageView imgBack;
    CircleImageView imgHinhNen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        imgBack=(ImageView) findViewById(R.id.imageBackMusic_31);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        MediaPlayer mediaPlayer=MediaPlayer.create(MusicActivity.this,R.raw.bong_hoa_dep_nhat);
        mediaPlayer.start();
        imgHinhNen=(CircleImageView) findViewById(R.id.hinhNen);
        final Animation animRotate= AnimationUtils.loadAnimation(this,R.anim.anim_roate);
        imgHinhNen.startAnimation(animRotate);
    }
}