package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class OnBoardingActivity extends AppCompatActivity {
    private OnBoardAdapter onBoardAdapter;
    ViewPager2 onBoardingViewPager;
    LinearLayout layoutOnboardIndicator;
    private MaterialButton btnAction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_boarding);
        setupOnboardingItems();
        onBoardingViewPager=(ViewPager2) findViewById(R.id.onBoardviewPager);
        layoutOnboardIndicator=(LinearLayout) findViewById(R.id.layoutOnboarding);
        btnAction=(MaterialButton) findViewById(R.id.buttonOnBoardaction);
        onBoardingViewPager.setAdapter(onBoardAdapter);
        setupOnboardingIndicators();
        setCurrentOnboardingIndicator(0);
        onBoardingViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                setCurrentOnboardingIndicator(position);
            }
        });
        btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onBoardingViewPager.getCurrentItem()+1<onBoardAdapter.getItemCount())
                {
                    onBoardingViewPager.setCurrentItem(onBoardingViewPager.getCurrentItem()+1);
                }else {
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    finish();
                }
            }
        });
    }
    private void setupOnboardingItems()
    {
        List<OnBoardItem> onBoardItems=new ArrayList<>();
        OnBoardItem itemPayDiemthuong=new OnBoardItem();
        itemPayDiemthuong.setTitle("Chương trình điểm thưởng");
        itemPayDiemthuong.setDescription("Thưởng thức cafe hay chơi game đều được tích điểm");
        itemPayDiemthuong.setImage(R.drawable.doithuong);

        OnBoardItem itemGiaohang=new OnBoardItem();
        itemGiaohang.setTitle("Giao hàng tận nơi");
        itemGiaohang.setDescription("Đặt món giao hàng tận nơi trong vòng 30 phút");
        itemGiaohang.setImage(R.drawable.dathang);

        OnBoardItem itemGoimon=new OnBoardItem();
        itemGoimon.setTitle("Gọi món tại bàn");
        itemGoimon.setDescription("Không cần xếp hàng, đặc quyền khác biệt!");
        itemGoimon.setImage(R.drawable.goimon);

        OnBoardItem itemTichdiem=new OnBoardItem();
        itemTichdiem.setTitle("Tích điểm đổi quà");
        itemTichdiem.setDescription("Dùng điểm đổi hàng ngàn ưu đãi hấp dẫn");
        itemTichdiem.setImage(R.drawable.doiqua);

        onBoardItems.add(itemPayDiemthuong);
        onBoardItems.add(itemGiaohang);
        onBoardItems.add(itemGoimon);
        onBoardItems.add(itemTichdiem);

        onBoardAdapter=new OnBoardAdapter(onBoardItems);
    }
    private void setupOnboardingIndicators()
    {
        ImageView[] indicators=new ImageView[onBoardAdapter.getItemCount()];
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.setMargins(8,0,8,0);
        for(int i=0;i< indicators.length;i++)
        {
            indicators[i]= new ImageView(getApplicationContext());
            indicators[i].setImageDrawable(ContextCompat.getDrawable(
                    getApplicationContext(),
                    R.drawable.onboard_indicator_inactive
            ));
            indicators[i].setLayoutParams(layoutParams);
            layoutOnboardIndicator.addView(indicators[i]);
        }
    }
    private void setCurrentOnboardingIndicator(int index){
        int childCount=layoutOnboardIndicator.getChildCount();
        for(int i=0;i<childCount;i++)
        {
            ImageView imageView=(ImageView)layoutOnboardIndicator.getChildAt(i);
            if(i==index)
            {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.onboarding_indicator_active)
                );
            }else
            {
                imageView.setImageDrawable(
                        ContextCompat.getDrawable(getApplicationContext(),R.drawable.onboard_indicator_inactive)
                );
            }
        }
        if (index==onBoardAdapter.getItemCount()-1)
        {
            btnAction.setText("Bắt đầu trải nghiệm");
        }else {
            btnAction.setText("Tiếp tục");
        }
    }


}
