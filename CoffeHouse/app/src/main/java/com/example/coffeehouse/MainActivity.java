package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ProgressBar pbBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pbBar=(ProgressBar) findViewById(R.id.progressBar);
        final CountDownTimer countDownTimer=new CountDownTimer(10000,300) {
            @Override
            public void onTick(long l) {
                int number=10;
                Random random=new Random();
                int so=random.nextInt(number);
                int current;
                current = pbBar.getProgress();





                pbBar.setProgress(current + so);
            }

            @Override
            public void onFinish() {
                //chạy nữa
                Intent myIntent=new Intent(MainActivity.this, OnBoardingActivity.class);
                startActivity(myIntent);
            }
        };
        countDownTimer.start();
    }
}