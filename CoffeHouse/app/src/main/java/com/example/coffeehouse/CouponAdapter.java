package com.example.coffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.List;

public class CouponAdapter  extends BaseAdapter {
    private Context context;
    int layout;
    List<Coupon> couponList;

    public CouponAdapter(Context context, int layout, List<Coupon> couponList) {
        this.context = context;
        this.layout = layout;
        this.couponList = couponList;
    }

    @Override
    public int getCount() {
        return couponList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);

        TextView txt_coupon_noidung_31=(TextView) view.findViewById(R.id.text_coupon_noidung);
        TextView txt_coupon_kethuc_31=(TextView) view.findViewById(R.id.text_coupon_ketthuc);
        ImageView imgHinh_coupon_31=(ImageView) view.findViewById(R.id.image_coupon_anh);
        Coupon coupon=couponList.get(i);
        DatabaseReference mData;
        DecimalFormat format=new DecimalFormat("###,###.###");
        txt_coupon_noidung_31.setText(coupon.getThongtin());
        txt_coupon_kethuc_31.setText(coupon.getKetthuc());
        Picasso.get().load(coupon.getAnh()).into(imgHinh_coupon_31);
        return view;
    }
}
