package com.example.coffeehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class TuDenLayActivity extends AppCompatActivity {
    ImageView img_close_tudenlay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tu_den_lay);
        img_close_tudenlay=(ImageView) findViewById(R.id.image_close_tudenlay);
        img_close_tudenlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(TuDenLayActivity.this,GiaoHangActivity.class));
            }
        });
    }
}