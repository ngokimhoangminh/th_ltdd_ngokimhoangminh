package com.example.coffeehouse;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ThucUongFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ThucUongFragment extends Fragment {
    ArrayList<Product> arrayProduct,arrthuocuongdaxay,arrMacchiato,arrChoco;
    ArrayList<GioHang> arrGioHang;
    GirdviewAdapter adapter,adapter2,adapter3,adapter4;
    ExpandableHeightGridView gridViewCaphe,gridViewThuocuongdaxay,gridviewMacchiato,gridviewChoco;
    DatabaseReference reference,mData,mData2,mData3,mData_giohang;
    ImageView imgHeart,imgPlus,imgMinus;
    TextView txtSoluong,txtTongtien,dl_txtSoluong, dl_txtTongTien;
    int total=0,tongtien=0,tongsoluong=0,tongsotien=0,tienthem=0,giatien=0,giatongtien=0,key=0;
    RelativeLayout re_xemgiohang_thucuong_31;
    String loai;
    CheckBox cb1,cb2,cb3;
    Boolean permission;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ThucUongFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ThucUongFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ThucUongFragment newInstance(String param1, String param2) {
        ThucUongFragment fragment = new ThucUongFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_thuc_uong, container, false);
        gridViewCaphe=(ExpandableHeightGridView) view.findViewById(R.id.girdviewCaPhe);
        gridViewThuocuongdaxay=(ExpandableHeightGridView) view.findViewById(R.id.girdviewThucUongdaxay);
        gridviewMacchiato=(ExpandableHeightGridView) view.findViewById(R.id.girdviewMacchiato);
        gridviewChoco=(ExpandableHeightGridView) view.findViewById(R.id.girdviewChoccoMatcha);
        re_xemgiohang_thucuong_31=(RelativeLayout) view.findViewById(R.id.xemgiohang_thucuong);
        dl_txtSoluong=(TextView) view.findViewById(R.id.dl_textsoluong);
        dl_txtTongTien=(TextView) view.findViewById(R.id.dl_textTongTien);
        arrayProduct=new ArrayList<Product>();
        arrthuocuongdaxay=new ArrayList<Product>();
        arrMacchiato=new ArrayList<Product>();
        arrChoco=new ArrayList<Product>();
        arrGioHang=new ArrayList<GioHang>();
        mData_giohang= FirebaseDatabase.getInstance().getReference();
        reference= FirebaseDatabase.getInstance().getReference().child("caphes");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot1 : snapshot.getChildren())
                {
                    Product p=dataSnapshot1.getValue(Product.class);
                    arrayProduct.add(p);
                }
                adapter=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrayProduct);
                gridViewCaphe.setAdapter(adapter);
                gridViewCaphe.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });

        LoadThucuongdaxay();
        LoadThucuongMacchiato();
        LoadThucuongChoco();
        LoadGioHang();

            re_xemgiohang_thucuong_31.setVisibility(View.GONE);
            re_xemgiohang_thucuong_31.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(getContext(),GioHangActivity.class));
                }
            });


        //xử lú click
        gridViewCaphe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    DialogThucUong(arrayProduct,i);
            }
        });
        gridViewThuocuongdaxay.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    DialogThucUong(arrthuocuongdaxay,i);
            }
        });
        gridviewMacchiato.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    DialogThucUong(arrMacchiato,i);
            }
        });
        gridviewChoco.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                    DialogThucUong(arrChoco,i);
            }
        });
        ExpandableHeightGridView gridViewCaphe=new ExpandableHeightGridView(getContext());
            gridViewCaphe.setNumColumns(2);
            gridViewCaphe.setAdapter(adapter);
            gridViewCaphe.setAdapter(adapter2);
             gridViewCaphe.setExpanded(true);
        return view;
    }
    private void  LoadGioHang()
    {
        mData_giohang.child("GioHang").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {
                GioHang gh=snapshot.getValue(GioHang.class);
                arrGioHang.add(new GioHang(gh.getTen(),gh.getSize(),gh.getMieuta(),gh.getHinhanh(),gh.getKey(),gh.getSoluong(),gh.getGiatien(),gh.isPermission()));
                re_xemgiohang_thucuong_31.setVisibility(View.VISIBLE);
                tongsoluong=0;
                tongsotien=0;
                for (int i=0;i<arrGioHang.size();i++){
                    tongsotien+=arrGioHang.get(i).getGiatien();
                    tongsoluong+=arrGioHang.get(i).getSoluong();
                }
                DecimalFormat format=new DecimalFormat("###,###.###");
                dl_txtSoluong.setText(tongsoluong+"");
                dl_txtTongTien.setText(format.format(tongsotien));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot snapshot, @Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    private void LoadThucuongdaxay()
    {
        mData= FirebaseDatabase.getInstance().getReference().child("thucuongdaxays");
        mData.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot2 : snapshot.getChildren())
                {
                    Product p=dataSnapshot2.getValue(Product.class);
                    arrthuocuongdaxay.add(p);
                }
                adapter2=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrthuocuongdaxay);
                gridViewThuocuongdaxay.setAdapter(adapter2);
                gridViewThuocuongdaxay.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadThucuongMacchiato()
    {
        mData2= FirebaseDatabase.getInstance().getReference().child("macchiatos");
        mData2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot3 : snapshot.getChildren())
                {
                    Product p=dataSnapshot3.getValue(Product.class);
                    arrMacchiato.add(p);
                }
                adapter3=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrMacchiato);
                gridviewMacchiato.setAdapter(adapter3);
                gridviewMacchiato.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void LoadThucuongChoco()
    {
        mData3= FirebaseDatabase.getInstance().getReference().child("chocomatcha");
        mData3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot dataSnapshot4 : snapshot.getChildren())
                {
                    Product p=dataSnapshot4.getValue(Product.class);
                    arrChoco.add(p);
                }
                adapter4=new GirdviewAdapter(getContext(),R.layout.gridview_item,arrChoco);
                gridviewChoco.setAdapter(adapter4);
                gridviewChoco.setExpanded(true);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(getContext(),".....something be wrong",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void DialogThucUong(ArrayList<Product> mangThucUong,int vitri)
    {
        final Dialog dialog=new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setContentView(R.layout.dialog_dathang);
        //dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        //
        RelativeLayout reTieudesize=(RelativeLayout) dialog.findViewById(R.id.tieudesize);
        LinearLayout lnKhungsize=(LinearLayout) dialog.findViewById(R.id.khungsize);
        //
        final TextView txtMieuta=(TextView) dialog.findViewById(R.id.dl_textmota);
        final TextView txtTen=(TextView) dialog.findViewById(R.id.dl_textTen);
        final TextView txtGia=(TextView) dialog.findViewById(R.id.dl_textGia);
        final ImageView imghinh=(ImageView) dialog.findViewById(R.id.dl_imagehinh);
        imgHeart=(ImageView) dialog.findViewById(R.id.imageHeart);
        txtTongtien=(TextView) dialog.findViewById(R.id.textviewTongtien);
        imgMinus=(ImageView) dialog.findViewById(R.id.imageViewMinus);
        imgPlus=(ImageView) dialog.findViewById(R.id.imageviewPlus);
        txtSoluong=(TextView) dialog.findViewById(R.id.textviewSoluong);
        final TextView txt_loai_nho_31=(TextView) dialog.findViewById(R.id.text_loai_nho);
        final TextView txt_loai_vua_31=(TextView) dialog.findViewById(R.id.text_loai_vua);
        final TextView txt_loai_lon_31=(TextView) dialog.findViewById(R.id.text_loai_lon);
        cb1=(CheckBox) dialog.findViewById(R.id.checkbox1);
        cb2=(CheckBox) dialog.findViewById(R.id.checkbox2);
        cb3=(CheckBox) dialog.findViewById(R.id.checkbox3);
        cb1.setChecked(true);
        final LinearLayout lnTongTien=(LinearLayout) dialog.findViewById(R.id.dl_linearTongTien);
        if(mangThucUong.get(vitri).isPermission())
        {
            reTieudesize.setVisibility(View.GONE);
            lnKhungsize.setVisibility(View.GONE);
            permission=true;
        }else
        {
            permission=false;
            cb1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb2.setChecked(false);
                    cb3.setChecked(false);
                }
            });
            cb2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb1.setChecked(false);
                    cb3.setChecked(false);
                }
            });
            cb3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    cb2.setChecked(false);
                    cb1.setChecked(false);
                }
            });
        }
        giatien=mangThucUong.get(vitri).getMoney();
        if(cb1.isChecked())
        {
            giatongtien=giatien;
        }
        if(cb2.isChecked())
        {
            giatongtien=giatien+7000;
        }
        if(cb3.isChecked())
        {
            giatongtien=giatien+14000;
        }
        /////////////////////////////////////////////
        txtMieuta.setText(mangThucUong.get(vitri).getDescrition());
        txtTen.setText(mangThucUong.get(vitri).getName());
        DecimalFormat format=new DecimalFormat("###,###.###");
        txtGia.setText(format.format(giatien));
        txtTongtien.setText(format.format(giatongtien)+" đ");
        Picasso.get().load(mangThucUong.get(vitri).getImage())
                .placeholder(R.drawable.tra1)
                .error(R.drawable.tra2)
                .into(imghinh);
        //imghinh.set(arrayPhobien.get(vitri).getImage());
        key++;
        lnTongTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ten=mangThucUong.get(vitri).getName();
                String mieuta=mangThucUong.get(vitri).getDescrition();
                String link=mangThucUong.get(vitri).getImage();
                int soluong=Integer.parseInt(txtSoluong.getText().toString());

                if(soluong>1)
                {
                    tongtien=total;
                }else
                {
                    tongtien=mangThucUong.get(vitri).getMoney();
                }
                if(cb1.isChecked())
                {
                    loai=txt_loai_nho_31.getText().toString();
                }
                if (cb2.isChecked())
                {
                    loai=txt_loai_vua_31.getText().toString();
                    tienthem=7000*soluong;
                }
                if (cb3.isChecked())
                {
                    loai=txt_loai_lon_31.getText().toString();
                    tienthem=14000*soluong;
                }
                tongtien+=tienthem;
                String tenkey="giohangthucuong"+key;
                GioHang giohang=new GioHang(ten,loai,mieuta,link,tenkey,soluong,tongtien,permission);
                mData_giohang.child("GioHang").child(tenkey).setValue(giohang);
                Handler handler=new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //Toast.makeText(getContext(),ten+" đã được thêm vào giỏ hàng" ,Toast.LENGTH_LONG).show();
                        displayMessage(ten+" đã được đưa vào giỏ hàng");
                        tongsoluong=0;
                        tongsotien=0;
                        for (int i=0;i<arrGioHang.size();i++){
                            tongsotien+=arrGioHang.get(i).getGiatien();
                            tongsoluong+=arrGioHang.get(i).getSoluong();
                        }
                        DecimalFormat format=new DecimalFormat("###,###.###");
                        dl_txtSoluong.setText(tongsoluong+"");
                        dl_txtTongTien.setText(format.format(tongsotien));
                        dialog.dismiss();
                    }
                },1000);

            }
        });

        imgHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                yeuthich();
            }
        });
        imgPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgMinus.setImageResource(R.drawable.ic_minus);
                int soluong=Integer.parseInt(txtSoluong.getText().toString());
                soluong++;
                txtSoluong.setText(soluong+"");
                int gia=mangThucUong.get(vitri).getMoney();
                total=0;
                total=gia*soluong;
                txtTongtien.setText(format.format(total)+" đ");
            }
        });
       imgMinus.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               int soluong=Integer.parseInt(txtSoluong.getText().toString());
               if(soluong>1)
               {
                   soluong--;
                   txtSoluong.setText(soluong+"");
                   int gia=mangThucUong.get(vitri).getMoney();
                   int total=0;
                   total=gia*soluong;
                   txtTongtien.setText(format.format(total)+" đ");
               }
               if(soluong==1)
               {
                   imgMinus.setImageResource(R.drawable.minuss);
               }
           }
       });
        dialog.show();
    }
    private  void displayMessage(String message)
    {
        Snackbar.make(re_xemgiohang_thucuong_31,message,Snackbar.LENGTH_SHORT).show();
    }
    int status=1;
    private  void yeuthich()
    {

        if(status==1)
        {
            status=2;
            imgHeart.setImageResource(R.drawable.heart2);
        }else
        {
            status=1;
            imgHeart.setImageResource(R.drawable.ic_heart);
        }
    }

}