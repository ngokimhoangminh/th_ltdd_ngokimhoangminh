package com.example.coffeehouse;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class GirdviewAdapter extends BaseAdapter {
    private Context context;
    int layout;
    List<Product> productList;

    public GirdviewAdapter(Context context, int layout, List<Product> productList) {
        this.context = context;
        this.layout = layout;
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);

        TextView txtName=(TextView) view.findViewById(R.id.textTen);
        TextView txtMoney=(TextView) view.findViewById(R.id.TextviewGiaTien);
        ImageView imgImage=(ImageView) view.findViewById(R.id.imageHinhSanPham);
        TextView txtDescription=(TextView) view.findViewById(R.id.textmota);
        ImageView imgThem=(ImageView) view.findViewById(R.id.imageThem);

        imgThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        Product product=productList.get(i);

        txtName.setText(product.getName());
       // Locale localeVN = new Locale("vi", "VN");
        //NumberFormat currencyVN = NumberFormat.getCurrencyInstance(localeVN);
        //String tien= currencyVN.format(product.getMoney());
        DecimalFormat format=new DecimalFormat("###,###.###");
        txtMoney.setText(format.format(product.getMoney()));
        txtDescription.setText(product.getDescrition());
        Picasso.get().load(product.getImage()).into(imgImage);

        return view;
    }
}

