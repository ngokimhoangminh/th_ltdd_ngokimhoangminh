package com.example.coffeehouse;

public class Coupon {
    public String anh;
    public String batdau;
    public String ketthuc;
    public String thongtin;

    public Coupon() {
    }

    public Coupon(String anh, String batdau, String ketthuc, String thongtin) {
        this.anh = anh;
        this.batdau = batdau;
        this.ketthuc = ketthuc;
        this.thongtin = thongtin;
    }

    public String getAnh() {
        return anh;
    }

    public void setAnh(String anh) {
        this.anh = anh;
    }

    public String getBatdau() {
        return batdau;
    }

    public void setBatdau(String batdau) {
        this.batdau = batdau;
    }

    public String getKetthuc() {
        return ketthuc;
    }

    public void setKetthuc(String ketthuc) {
        this.ketthuc = ketthuc;
    }

    public String getThongtin() {
        return thongtin;
    }

    public void setThongtin(String thongtin) {
        this.thongtin = thongtin;
    }
}
