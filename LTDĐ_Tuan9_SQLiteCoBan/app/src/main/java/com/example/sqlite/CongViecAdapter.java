package com.example.sqlite;

import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CongViecAdapter extends BaseAdapter {
    private int layout;
    private MainActivity context;
    private List<CongViec> congViecList;

    public CongViecAdapter(int layout, MainActivity context, List<CongViec> congViecList) {
        this.layout = layout;
        this.context = context;
        this.congViecList = congViecList;
    }

    @Override
    public int getCount() {
        return congViecList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    private  class ViewHolder{
        TextView txtTen;
        ImageView imgDelete,imgUpdate;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
                if(view==null)
                {
                    holder=new ViewHolder();
                    LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    view=inflater.inflate(layout,null);
                    holder.txtTen=(TextView) view.findViewById(R.id.textViewTenCongViec);
                    holder.imgDelete=(ImageView) view.findViewById(R.id.imageViewDelete);
                    holder.imgUpdate=(ImageView) view.findViewById(R.id.imageViewUpdate);
                    view.setTag(holder);
                }
                else
                {
                    holder=(ViewHolder) view.getTag();
                }
                final CongViec congViec=congViecList.get(i);
                holder.txtTen.setText(congViec.getTenCV());

                holder.imgUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        context.dialogSua(congViec.getTenCV(),congViec.getIdCV());
                    }
                });
                holder.imgDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.DialogXoa(congViec.getTenCV(),congViec.getIdCV());
                    }
                });
        return view;
    }
}
