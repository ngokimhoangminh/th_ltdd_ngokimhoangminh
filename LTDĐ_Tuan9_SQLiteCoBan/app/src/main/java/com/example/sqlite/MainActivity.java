package com.example.sqlite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.ListViewAutoScrollHelper;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Database database;
    ListView lvcongViec;
    ArrayList<CongViec> arrayCongViec;
    CongViecAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvcongViec=(ListView) findViewById(R.id.listviewCongViec);
        arrayCongViec=new ArrayList<>();
        adapter=new CongViecAdapter(R.layout.dong_cong_viec, this,arrayCongViec);
        lvcongViec.setAdapter(adapter);
        //tạo database
        database=new Database(this,"ghichu.sqlite",null,1);

        //tạo bảng CongViec
        database.QueryData("CREATE TABLE IF NOT EXISTS CongViec(Id INTEGER PRIMARY KEY AUTOINCREMENT, TenCV VARCHAR(200))");

        //insert Data
       //database.QueryData("INSERT into CongViec values(null,'Viết ứng dụng ghi chú')");

        //select data
        getdataCongViec();
    }
    public void DialogXoa(final String tenCV, final int id)
    {
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
                builder.setTitle("Xác nhận xóa");
                builder.setIcon(R.drawable.ic_delete);
                builder.setMessage("Bạn có chắc chắn muốn xóa 'Công Việc' này");

                builder.setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        database.QueryData("DELETE FROM CongViec WHERE Id='"+ id+"'");
                        Toast.makeText(MainActivity.this,"Đã xóa "+ tenCV+" thành công",Toast.LENGTH_LONG).show();
                        getdataCongViec();
                    }
                });
                builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });

                builder.show();

    }
    public void dialogSua(String ten, final int id)
    {
        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        dialog.setContentView(R.layout.dialog_sua);
        Button btnLuu=(Button) dialog.findViewById(R.id.ButtonLuu);
        Button btncancel=(Button) dialog.findViewById(R.id.ButtonHuy);
        final EditText edtTenCV=(EditText) dialog.findViewById(R.id.EdittextTen);
        edtTenCV.setText(ten);
        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ten=edtTenCV.getText().toString().trim();
                database.QueryData("UPDATE CongViec SET TenCv='"+ ten+"' where Id='"+id +"'");
                Toast.makeText(MainActivity.this,"Đã cập nhật thành công",Toast.LENGTH_LONG).show();
                dialog.dismiss();
                getdataCongViec();
            }
        });
        btncancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();


    }
    public void getdataCongViec()
    {

        Cursor dataCongViec= database.getData("SELECT * FROM CongViec");
        arrayCongViec.clear();
        while (dataCongViec.moveToNext()==true)
        {
            String ten=dataCongViec.getString(1);
            int id=dataCongViec.getInt(0);

            arrayCongViec.add(new CongViec(id,ten));
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_congviec,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId()==R.id.menuThem)
        {
            themDialog();
        }
        return super.onOptionsItemSelected(item);
    }
    public void themDialog()
    {

        final Dialog dialog=new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);//xóa title ở trên setContentView
        dialog.setCanceledOnTouchOutside(false);//click bên ngoài không tắt dialog
        dialog.setContentView(R.layout.dialog_them);

        Button btnThem=(Button) dialog.findViewById(R.id.ButtonThem);
        Button btnHuy=(Button) dialog.findViewById(R.id.ButtonCancel);
        final EditText edtTen=(EditText) dialog.findViewById(R.id.EdittextName);

        btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    String ten=edtTen.getText().toString();
                    if(ten.equals(""))
                    {
                        Toast.makeText(MainActivity.this,"Bạn chưa chọn tên công việc",Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        database.QueryData("INSERT INTO CongViec VALUES(null,'"+ten+"')");
                        Toast.makeText(MainActivity.this,"Thêm thành công",Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        getdataCongViec();
                    }
            }
        });
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}