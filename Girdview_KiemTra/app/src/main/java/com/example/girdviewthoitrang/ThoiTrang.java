package com.example.girdviewthoitrang;

public class ThoiTrang {
    private String name;
    private String price;
    private String discount;
    private String oldPrice;
    private int image;

    public ThoiTrang(String name, String price, String discount, String oldPrice, int image) {
        this.name = name;
        this.price = price;
        this.discount = discount;
        this.oldPrice = oldPrice;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
