package com.example.girdviewthoitrang;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.NotificationManager;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.audiofx.BassBoost;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.nex3z.notificationbadge.NotificationBadge;
import com.txusballesteros.bubbles.BubbleLayout;
import com.txusballesteros.bubbles.BubblesManager;
import com.txusballesteros.bubbles.OnInitializedCallback;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    GridView gridView;
    ArrayList<ThoiTrang> mangThoiTrang;
    MyAdapter adapter;
    VideoView videoView;
    BubblesManager bubblesManager;
    BottomNavigationView navigationView;
    private  int MY_PERMISSION=1000;
    int vitri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Anhxa();
        adapter=new MyAdapter(this,R.layout.dong_layout,mangThoiTrang);
        gridView.setAdapter(adapter);

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                xacnhanXoa(i);
                return false;
            }
        });
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(MainActivity.this,DetailActivity.class);
                startActivity(intent);
            }
        });

    }


    private void xacnhanXoa(final int position)
    {
        AlertDialog.Builder alertDialog=new AlertDialog.Builder(this);
        alertDialog.setTitle("Thông Báo!");
        alertDialog.setIcon(R.mipmap.ic_launcher);
        alertDialog.setMessage("Bạn có muốn xóa môn học này");
        alertDialog.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mangThoiTrang.remove(position);
                adapter.notifyDataSetChanged();//cập nhậ lại mảng
            }
        });
        alertDialog.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alertDialog.show();
    }


    public  void Anhxa()
    {
        gridView=(GridView) findViewById(R.id.girdview);
        mangThoiTrang=new ArrayList<>();
        mangThoiTrang.add(new ThoiTrang("Loa Karaoke và nghe nhạc 3 tấc thùng sươn KP","đ2.680.000","-46%","đ5.000.000",R.drawable.dt1));
        mangThoiTrang.add(new ThoiTrang("Loa Karaoke LG Xboom RN7 (500W) chính hãng","đ3.990.000","-33%","đ5.990.000",R.drawable.dt2));
        mangThoiTrang.add(new ThoiTrang("Loa Karaoke và nghe nhạc 3 tấc thùng sươn KP","đ2.680.000","-46%","đ5.000.000",R.drawable.dt3));
        mangThoiTrang.add(new ThoiTrang("Loa Karaoke và nghe nhạc 3 tấc thùng sươn KP","đ2.680.000","-46%","đ5.000.000",R.drawable.dt4));
    }


}