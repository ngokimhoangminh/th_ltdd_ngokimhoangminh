package com.example.girdviewthoitrang;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private Context context;
    int layout;
    List<ThoiTrang> thoiTrangList;

    public MyAdapter(Context context, int layout, List<ThoiTrang> thoiTrangList) {
        this.context = context;
        this.layout = layout;
        this.thoiTrangList = thoiTrangList;
    }

    @Override
    public int getCount() {
        return thoiTrangList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater= (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        view=inflater.inflate(layout,null);

        TextView txtName=(TextView) view.findViewById(R.id.textviewTen);
        TextView txtPrice=(TextView) view.findViewById(R.id.textviewGia);
        TextView txtDiscount=(TextView) view.findViewById(R.id.textviewGiamGia);
        ImageView imgImage=(ImageView) view.findViewById(R.id.imageviewHinh);
        TextView txtMoney=(TextView) view.findViewById(R.id.money);
        ThoiTrang thoiTrang=thoiTrangList.get(i);

        txtName.setText(thoiTrang.getName());
        txtPrice.setText(thoiTrang.getPrice());
        txtDiscount.setText(thoiTrang.getDiscount());
        imgImage.setImageResource(thoiTrang.getImage());
        txtMoney.setText(thoiTrang.getOldPrice());
        return view;
    }
}
